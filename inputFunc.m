function inputFunc(app,varargin)
%author: Jeppe J. Thuesen, jeppethuesen@gmail.com

%This function is used in order to load the lab specific settings, which is
%mostly useful when the app gets an update, then it should not be required
%to "set up" the app at different computers.
%In case you want more features to be preselected, simply add them in here
%and in your dawn file.


input = varargin{1};

switch input.lab
    case 'Lattice'
        app.LabDropDown.Value = input.lab;
%         app.constants.cameraCalib = input.cameraCalib;
%         app.constants.detectionTime = input.detectionTime;
        app.DriveDropDown.Value = input.drive;
        app.SpeciesDropDown.Value = input.species;
        
        
    case 'MIX'
        app.LabDropDown.Value = input.lab;
%         app.constants.cameraCalib = input.cameraCalib;
%         app.constants.detectionTime = input.detectionTime;
        app.DriveDropDown.Value = input.drive;
        app.SpeciesDropDown.Value = input.species;
        
        
    case 'Hi Res'
        app.LabDropDown.Value = input.lab;
        app.DriveDropDown.Value = input.drive;
        app.SpeciesDropDown.Value = input.species;
end



end