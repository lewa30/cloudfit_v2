


rt = readtable('skyFitRampD3.txt');


%% Standard export data
runNumber = rt.RunNumber;
Time      = rt.Time;
NSum      = rt.NSum;
NBECSum   = rt.NBECSum;
Nthermal  = rt.Nthermal;
NBEC      = rt.NBEC;
Tc        = rt.Tc;
TX        = rt.TX;
TY        = rt.TY;
TXTc      = rt.TXTc;
TYTc      = rt.TYTc;
xPos      = rt.xPos;
yPos      = rt.yPos;
xRadius   = rt.xRadius;
yRadius   = rt.yRadius;
psdX      = rt.psdX;
psdY      = rt.psdY;