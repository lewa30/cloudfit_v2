%% The results shown in the file is calculated
function calculateCloudProperties(obj)


if obj.fitFail == 1
    return
end

%% Extract fitting results from thermal fit
   thermalCoefficientNames = {'Volume', 'xCenter', 'xWidth', 'yCenter', 'yWidth', 'offset', 'fugacity'};
   
   %Create result table
   thermalResult = table('RowNames', thermalCoefficientNames);
   thermalResult.Estimate = zeros(length(thermalCoefficientNames), 1);
   thermalResult.Estimate('fugacity', :) = NaN;
   
   %Question: Was there done a thermal fit?
if isempty(obj.foThermal) == false
   thermalResult{obj.foThermal.CoefficientNames, 'Estimate'}...
      = obj.foThermal.Coefficients{obj.foThermal.CoefficientNames, 'Estimate'};
end
%    % Yes ->
%    % Create result table
%    thermalResult = table('RowNames', obj.foThermal.CoefficientNames);
%    thermalResult.Estimate = zeros(length(obj.foThermal.CoefficientNames), 1);
%    
%    % Pass the fitted coefficients
%    
% end

%% Extract fitting results from Thomas-Fermi fit
   becCoefficientNames = {'Volume', 'xCenter', 'xRadius', 'yCenter', 'yRadius', 'offset'};
   
   becResult = table('RowNames', becCoefficientNames);
   becResult.Estimate = zeros(length(becCoefficientNames), 1);
   
if isempty(obj.foBec) == false
   becResult{obj.foBec.CoefficientNames, 'Estimate'}...
   = obj.foBec.Coefficients{obj.foBec.CoefficientNames, 'Estimate'};   
end


%% Determine offset
if all( [thermalResult{'offset',1}, becResult{'offset',1}] > 0 )
   error('UltracoldImage:CalculateCloudProperties:OffsetDoublyFitted', ...
      'The chosen model fits an offset twice - this will lead to wrong results')
end
offset = thermalResult{'offset',1} + becResult{'offset',1};


%% Scaling coefficients
od2atomNumber = obj.parameters.pixelSize^2/obj.parameters.crosssection;

widthSquared2temperatureX = obj.parameters.mass/obj.kB/...
    (1/obj.parameters.omegaX^2 + obj.parameters.ToF^2);

widthSquared2temperatureY = obj.parameters.mass/obj.kB/...
    (1/obj.parameters.omegaY^2 + obj.parameters.ToF^2);

%% Calculate results

obj.results.RunNumber   = obj.run;
obj.results.Date        = obj.date;


% Calculate summing results
obj.results.NSum        = (sum(obj.od(obj.Roi.cloud))...
        -sum(sum(obj.Roi.cloud))*offset)*od2atomNumber;

if isempty(obj.foThermal) == true && isempty(obj.foBec) == true
    return
end

% %
% if obj.results.NSum < 0
%     obj.failedFit
%     obj.fitFail = 1;
%     
%     disp('Error: NSum is negative')
%     
%     return
% end

if isempty(obj.foThermal) == false
   
   obj.results.NBECSum ...
      = sum(obj.od(logical(obj.Roi.bec)) ...
      - obj.foThermal.feval(obj.X(logical(obj.Roi.bec)), obj.Y(logical(obj.Roi.bec))))*od2atomNumber;
else
   obj.results.NBECSum = sum(obj.od(obj.Roi.bec))*od2atomNumber;
end

obj.results.Nthermal ...
   = thermalResult{'Volume', 'Estimate'}*od2atomNumber;

obj.results.NBEC    = becResult{'Volume', 'Estimate'}*od2atomNumber;


%% Temperature results
obj.results.Tc = obj.hbar*(obj.parameters.omegaX*obj.parameters.omegaY*obj.parameters.omegaZ)^(1/3)...
   *obj.results.NSum^(1/3)/(zeta(3)^(1/3)*obj.kB);


obj.results.TX = (thermalResult{'xWidth', 'Estimate'})^2 ...
   *obj.parameters.pixelSize^2*widthSquared2temperatureX;

obj.results.TY = (thermalResult{'yWidth', 'Estimate'})^2 ...
   *obj.parameters.pixelSize^2*widthSquared2temperatureY;


obj.results.TXTc = obj.results.TX/obj.results.Tc;
obj.results.TYTc = obj.results.TY/obj.results.Tc;

roundedY = round(obj.Roi.centerY);
roundedX = round(obj.Roi.centerX);

%So if we have a BEC we want the center of the atomic cloud to be displayed
%as the center of the BEC. If there is no BEC we want the center of the
%thermal cloud as our center.
switch obj.centerBoundness
    case 'Fitted'
            obj.results.xPosBEC = becResult{'xCenter','Estimate'};
            obj.results.yPosBEC = becResult{'yCenter','Estimate'};

            obj.results.xPosThermal = thermalResult{'xCenter','Estimate'};
            obj.results.yPosThermal = thermalResult{'yCenter','Estimate'};

    case 'Bound'
        obj.results.xPosBEC = obj.Roi.centerX;
        obj.results.yPosBEC = obj.Roi.centerY;
        obj.results.xPosThermal = obj.Roi.centerX;
        obj.results.yPosThermal = obj.Roi.centerY;     
end

obj.results.xRadiusBEC = becResult{'xRadius','Estimate'};
obj.results.yRadiusBEC = becResult{'yRadius','Estimate'};



obj.results.xRadiusThermal = thermalResult{'xWidth','Estimate'};
obj.results.yRadiusThermal = thermalResult{'yWidth','Estimate'};



if isempty(obj.foThermal) == false && isempty(obj.foBec) == true
    obj.results.psdX = obj.results.Nthermal*obj.hbar^3*...
        obj.parameters.omegaX*obj.parameters.omegaY*obj.parameters.omegaZ...
        /(obj.kB*obj.results.TX)^3;
    
    obj.results.psdY = obj.results.Nthermal*obj.hbar^3*...
        obj.parameters.omegaX*obj.parameters.omegaY*obj.parameters.omegaZ...
        /(obj.kB*obj.results.TY)^3;

    
%% For phase space density in the case of BE thermal  
%     if obj.hot == 0
%         obj.results.psdX = obj.results.psdX/zeta(3);
%         obj.results.psdY = obj.results.psdY/zeta(3);
%     end
    
   else
    obj.results.psdX = NaN;
    obj.results.psdY = NaN;
end


Ephoton = 2*pi*obj.hbar*obj.c/obj.lambda;

counts2photons = obj.parameters.cameraCalib * obj.parameters.pixelSize^2 / Ephoton *1e-6;

obj.results.Intensity = obj.Intensity;

 obj.results.NphPulse    =  sum( obj.raw.beam(obj.Roi.cloud) )*counts2photons;
 obj.results.NphAbs      =  sum( obj.raw.beam(obj.Roi.cloud)*obj.ratio  ...
     - obj.raw.beamBias(obj.Roi.cloud)*obj.ratio - (obj.raw.atoms(obj.Roi.cloud)...
     - obj.raw.atomBias(obj.Roi.cloud))  )*counts2photons;


sides = 3; % Changeable. This determines how many values to
%look at, in order to determine the error value.
peakRaw = obj.od(roundedY,roundedX-sides:roundedX+sides);

% if obj.BecOn == 1
%     
%     if becResult{'Volume','Estimate'}<0
%         %If the Volume is negative, the fit have failed
%         %therefore an extremely large number is used to say
%         %that it failed.
%         obj.results.midPointsError = 1e+20;
%         
%     else %If the volume seems okay
%         if obj.ThermalOn ==1
%             
%             peakFit = obj.foThermal.feval(roundedX-sides:roundedX+sides,roundedY)...
%                 +obj.foBec.feval(roundedX-sides:roundedX+sides,roundedY);
%             
%         else
%             peakFit = obj.foBec.feval(roundedX-sides:roundedX+sides,roundedY);
%             
%         end
%         obj.results.midPointsError = mean(((peakRaw-peakFit)./max(peakRaw)).^2);
%     end
% else %obj.BecOn == 0 && obj.ThermalOn == 1
%     peakFit = obj.foThermal.feval(roundedX-sides:roundedX+sides,roundedY);
%     obj.results.midPointsError = mean(((peakRaw-peakFit)./max(peakRaw)).^2);
% end



if isempty(obj.foBec) == 0
    obj.results.BECSSE     = obj.foBec.SSE;
else
    obj.results.BECSSE = NaN;
end
if isempty(obj.foThermal) == 0
    obj.results.thermalSSE = obj.foThermal.SSE;
else
    obj.results.thermalSSE = NaN;
end

obj.results.fitFail = obj.fitFail;
end