classdef UltracoldImage < handle
    %ULTRACOLDIMAGE Ultracold Image fitting object
    % obj = UltracoldImage( ImageSpecification )
    %
    %   Detailed explanation goes here
    %
    %     Orginal author: Mick Kristensen
    %     Latest author: Jeppe Thuesen (jeppethuesen@gmail.com)
    %     TODO:
    
    properties (SetAccess = public)
        %parameters
        parameters           = struct;
        BecOn
        ThermalOn
        
    end
    properties (SetAccess = private)
        % Image specification
        raw = struct;
        date				= NaT
        run					= NaN
        rot                 = 0;
        
        % Nature parameters
        kB                  = 1.380648520000000e-23;
        hbar                = 1.0545718e-34;
        c                   = 299792458;
        Isat                = 1.669*1e1; % W/m2
        lambda              = 780.246e-9;
        
        % Roi specification
        edgePixels			= [0 0 0 0]
        Roi                 = struct
        
        
        % Fitting parameters
        options				= struct
        startingGuess
        fitOffset           = 0
        
        %miscellaneous
        maxOD               = 4.5;
        becRoiMethod        = '';
        centerBoundness     = '';
        
        LatticeParameterLoading = 'Off';
        
        fitmodel
%         %On-parameters

%         semi
%         hot       
        
        %FitFail
        fitFail             = 0;
    end
    
    properties (SetAccess = private)
        ratio
        od
        results = table
        foThermal
        foBec
        X
        Y
        validThermalModels = [  "Maxwell-Boltzman"
                                "Hot thermal"
                                "Thermal"
                                "Bose-Einstein"
                                "Bose-enhanced"
                                "BE Thermal"
                                ]
        validBimodalModels = [  "Bimodal"
                                "Cold bimodal"]
        validBecModels     = [  "Thomas-Fermi"
                                "Pure BEC"]
        validMiscModels    = [  "None" ]
    end
    
    properties (SetAccess = private)
        Intensity
        IntensityStd
    end

    
    methods
        % Declaration of methods in separate files
        calculateCloudProperties(obj)
    end
    
    methods
        %% Constructor
        function obj = UltracoldImage( inputStruct)
            obj = obj.parseInputStructure( inputStruct );
%             obj.imageFormat;
            obj.loadRawImage;
            obj.calculateOpticalDensity;
            obj.createRoi;
            obj.performFit
            
%             if obj.hot == 0
%                 if obj.ThermalOn == 1
%                     obj.fitThermalDistribution;
%                 end
%                 
%                 if obj.BecOn == 1
%                     obj.fitThomasFermiDistribution;
%                 end
%             else
%                 obj.fitHotThermal;
%             end
            
            
            obj.calculateCloudProperties;
            
            
        end
    end
    
    methods (Access = private)
        function obj = parseInputStructure(obj, inputStruct )
            % Setup inputparser
            parser = inputParser;
            parser.FunctionName = 'UltracoldImage';
            
            % Setup validation functions
            chkScalar = @(x) validateattributes(x, {'numeric'}, {'finite', 'scalar'});
            chkVector = @(x,size) validateattributes(x, {'numeric'}, {'finite', 'numel', size});
            
            % Add parameters
            parser.addParameter('date', obj.date, @isdatetime)
            parser.addParameter('run', obj.run, @isnumeric)
            parser.addParameter('edgePixels', obj.edgePixels, @(x) chkVector(x, 4))
            parser.addParameter('options', obj.options, @isstuct)
            parser.addParameter('maxOD', obj.maxOD, @isnumeric);  %Could also be a string
            parser.addParameter('startingGuess', obj.startingGuess, @(x) validateattributes(x, {'numeric', 'struct'}, {}));
            parser.addParameter('fitmodel', obj.fitmodel);%, @(x) validatestring(x));
            parser.addParameter('Roi' ,obj.Roi, @(x) isa(x, 'ROI'));
            parser.addParameter('becRoiMethod', obj.becRoiMethod, @(x) any(validatestring(x, {'1D calculation','Load previous'})));
            parser.addParameter('centerBoundness',obj.centerBoundness, @(x) any(validatestring(x, {'Bound','Fitted'})));
            parser.addParameter('fitOffset',obj.fitOffset, @islogical);
            parser.addParameter('LatticeParameterLoading', obj.LatticeParameterLoading, @ischar);
            parser.addParameter('rot',obj.rot, @isnumeric);
            parser.addParameter('parameters',obj.parameters, @isstruct);
            parser.addParameter('raw',obj.raw, @isstruct);
            
            parser.parse( inputStruct );
            
            % Assign parser result to object properties
            parameterNames = fieldnames(parser.Results);
            nfileds        = numel(parameterNames);
            for ifield = 1:nfileds
                obj.(parameterNames{ifield}) = parser.Results.(parameterNames{ifield});
            end
        end
        
        function loadRawImage(obj)
            %% Lattice Parameter Loading
            switch obj.LatticeParameterLoading
                case 'On'
                    Time = datestr(obj.date);
%                     Time = obj.raw.dateData;
                    LatticeParameters(obj,Time,obj.run)
            end
        end
        
        %%
        function calculateOpticalDensity(obj)
            obj.raw = HiRes.rawRot(obj.raw,obj.rot);
            
            % Subtract bias images
            atoms = obj.parameters.cameraCalib*(obj.raw.atoms - obj.raw.atomBias)./obj.parameters.detectionTime;
            beam  = obj.parameters.cameraCalib*(obj.raw.beam  - obj.raw.beamBias)./obj.parameters.detectionTime;
                        
            % If there is zero-counts in the image the image we cannot calculate
            % od, so we add eps
            atoms(atoms == 0) = eps;
            beam(beam == 0) = eps;
            if isempty(obj.Roi.reference)
                obj.ratio = 1;
            else
                % Calculate ratio between atom and beam image and rescale coefficient
                obj.ratio  = mean(atoms(obj.Roi.reference))./mean(beam(obj.Roi.reference));
                %     ratioW = mean(alphaGO*lambertw(atoms(roi.reference).*exp(atoms(roi.reference)/alphaGO)/alphaGO)./beam(roi.reference))
            end
            beam   = beam*obj.ratio;
            
            obj.Intensity = mean(beam(obj.Roi.cloud));
            obj.IntensityStd = std(beam(obj.Roi.cloud));
            
            delta = log(abs(beam./atoms));
            
            % Calculate optical density using the Guery-Odelin formula if alpha is
            % numeric or by the function idea if alpha is given by a function handle
            if isnumeric(obj.parameters.alpha) == true
                obj.od = obj.parameters.alpha*delta + (beam - atoms);
            else
                alphaFx = reshape( obj.parameters.alpha(delta), size(delta));
                obj.od = alphaFx.*delta + (beam - atoms);
            end
            [obj.X, obj.Y] = meshgrid(1:size(obj.od,2),1:size(obj.od,1));
            disp('OD calculated')
        end
        
 
        %%
        function createRoi(obj)
            % DOCUMENT!
            if isempty(obj.Roi)
                obj.Roi = ROI;
            end
            if isempty(obj.Roi.cloud)
                obj.Roi.createCloudRoi(obj.od);
            end
%             if isempty(obj.Roi.reference)
%                 obj.Roi.createReferenceRoi;
%             end
           
            
        end
        
        %% 
        function performFit(obj)
            obj.ThermalOn = false;
            obj.BecOn     = false;
            semi          = false; % used for defining the BEC-RoI in the
            %"cold bimodal" fitting method.
            

            obj.Roi.picture   = obj.od.*obj.Roi.image;
            
            switch obj.fitmodel
                %road: Define if thermal and bec is on -->
                % Create the appropiate BEC and thermal RoIs -->
                %fit  appropiate models 
                case {"None"}
                    
                case {"Maxwell-Boltzman", "Hot thermal", "Thermal"}
                    obj.ThermalOn = true;
                    
                    obj.Roi.createBecAndThermalRoi(obj.becRoiMethod,obj.BecOn, obj.od, obj.X, obj.Y,semi, obj.maxOD);
                    obj.fitHotThermal;
                    
                    obj.Roi.centerX = obj.foThermal.Coefficients{'xCenter', 'Estimate'};
                    obj.Roi.centerY = obj.foThermal.Coefficients{'yCenter', 'Estimate'};

                    
                case {"Bose-Einstein", "Bose-enhanced", "BE Thermal"}
                    obj.ThermalOn = true;
                    
                    obj.Roi.createBecAndThermalRoi(obj.becRoiMethod,obj.BecOn, obj.od, obj.X, obj.Y,semi, obj.maxOD);
                    obj.fitThermalDistribution;
                    
                    obj.Roi.centerX = obj.foThermal.Coefficients{'xCenter', 'Estimate'};
                    obj.Roi.centerY = obj.foThermal.Coefficients{'yCenter', 'Estimate'};
                    
                case {"Bimodal"}
                    obj.ThermalOn = true;
                    obj.BecOn     = true;
                    becOffset     = false;
                    
                    
                    obj.Roi.createBecAndThermalRoi(obj.becRoiMethod,obj.BecOn, obj.od, obj.X, obj.Y,semi, obj.maxOD);
                    obj.fitThermalDistribution;
                    obj.fitThomasFermiDistribution(becOffset);


                case {"Cold bimodal"}                    
                    obj.ThermalOn = true;
                    obj.BecOn     = true;
                    semi          = true;
                    becOffset     = false;
                    
                    obj.Roi.createBecAndThermalRoi(obj.becRoiMethod,obj.BecOn, obj.od, obj.X, obj.Y,semi, obj.maxOD);
                    obj.fitThermalDistribution;
                    obj.fitThomasFermiDistribution(becOffset);

                    
                case {"Thomas-Fermi", "Pure BEC"}
                    obj.BecOn     = true;         
                    
                    if obj.fitOffset == true
                       becOffset = true;
                    else
                       becOffset = false;
                    end
                    
                    
                    obj.Roi.createBecAndThermalRoi(obj.becRoiMethod,obj.BecOn, obj.od, obj.X, obj.Y,semi, obj.maxOD);
                    obj.fitThomasFermiDistribution(becOffset);
                    
                otherwise
                    error('Fittingmodel unknown')
            end
             
                
          
                
       
        end
        
        
        
        %% --------------- FITTING METHODS ---------------
        function fitThermalDistribution(obj)
            peak = max(max(obj.Roi.picture));
            try
                switch obj.centerBoundness
                    case 'Bound'
                        obj.foThermal = ...
                            fittingModels.fit_ThermalDensityBound( obj.od, obj.Roi.thermal,obj.X,obj.Y, peak,obj.Roi.centerX,obj.Roi.centerY,obj.fitOffset, obj.options);
                    case 'Fitted'
                        obj.foThermal = ...
                            fittingModels.fit_ThermalDensity( obj.od, obj.Roi.thermal,obj.X,obj.Y, peak, obj.fitOffset, obj.options);
                end
            catch EM
%                                 rethrow(EM)
                disp('thermalFit failed')
                
                %This is used to cancel all further work, as to not run
                %into an error.
                obj.fitFail = 1;
                
                %This sets all calculated values to NaN
                obj.failedFit
                

                
            end
        end
        
        %%
        function fitThomasFermiDistribution(obj,becOffset)
            if obj.fitFail == 1
                return
            end
            
            if obj.ThermalOn == 1
                thermalInput = obj.foThermal.feval(obj.X(obj.Roi.bec), obj.Y(obj.Roi.bec));
            else
                if becOffset == true
                   thermalInput = -mean(mean(obj.od(obj.Roi.image & ~obj.Roi.bec))) * ones(size(obj.X(obj.Roi.bec)));
                else
                   thermalInput = zeros(size(obj.X(obj.Roi.bec)));
                end
                
            end
            try
                switch obj.centerBoundness
                    case 'Bound'
                        obj.foBec = ...
                            fittingModels.fit_BecDensityBound(obj.od, obj.Roi.bec, thermalInput, obj.X, obj.Y,obj.Roi.centerX,obj.Roi.centerY,becOffset);
                    case 'Fitted'
                        obj.foBec = ...
                            fittingModels.fit_BecDensity(obj.od, obj.Roi.bec, thermalInput, obj.X, obj.Y,becOffset);
                        
                end
            catch EM
%                                 rethrow(EM)
                disp('BEC fit failed')
%                 rethrow(EM)
                

                
                obj.fitFail = 1;
                %This sets all calculated values to NaN
                obj.failedFit
                
            end
            
        end
        
        
        %% For hot thermal clouds
        function fitHotThermal(obj)
            peak = max(max(obj.Roi.picture));
            try
                obj.foThermal = ...
                    fittingModels.pureGaussianFit( obj.od, obj.Roi.thermal,obj.X,obj.Y, peak,obj.fitOffset, obj.options);
                
            catch EM
%                 rethrow(EM)
                disp('thermalFit failed')
                
                
                
                %This is used to cancel all further work, as to not run
                %into an error.
                obj.fitFail = 1;
                
                
                %This sets all calculated values to NaN
                obj.failedFit
                

                
            end
            
            
        end
        
        function getResults(obj)
            %Unused
            inputStruct = struct('parameters', obj.parameters,...
                'runNumber',obj.run,...
                'OD', obj.od,...
                'ROI', obj.Roi,...
                'thermalOn', obj.ThermalOn,...
                'becOn', obj.BecOn,...
                'fitFail',obj.fitFail,...
                'fitOffset', obj.fitOffset,...
                'thermalModel',obj.foThermal,...
                'BECModel',obj.foBec)
            
        end
        
        %% If either thermal or BEC fit fails
        function failedFit(obj)
            
            
            od2atomNumber = obj.parameters.pixelSize^2/obj.parameters.crosssection;
            
            %             widthSquared2temperatureX = obj.mass/obj.kB/...
            %                 (1/obj.omegaX^2 + obj.timeOfFlight^2);
            %
            %             widthSquared2temperatureY = obj.mass/obj.kB/...
            %                 (1/obj.omegaY^2 + obj.timeOfFlight^2);
            %
            
            % Calculate summing results
            obj.results.RunNumber   = obj.run;
            obj.results.Date        = obj.date;
            obj.results.NSum        = sum(obj.od(obj.Roi.cloud))*od2atomNumber;
            
            
            % Sets every output to NaN as to avoid errors or numbers that
            % are certainly off.
            obj.results.NBECSum  = NaN;
            obj.results.Nthermal = NaN;
            obj.results.NBEC     = NaN;
            
            obj.results.Tc = obj.hbar*(obj.parameters.omegaX*obj.parameters.omegaY*obj.parameters.omegaZ)^(1/3)...
                *obj.results.NSum^(1/3)/(zeta(3)^(1/3)*obj.kB);
            
            obj.results.TX   = NaN;
            obj.results.TY   = NaN;
            obj.results.TXTc = NaN;
            obj.results.TYTc = NaN;
            
            obj.results.xPosBEC = NaN;
            obj.results.yPosBEC = NaN;
            
            obj.results.xPosThermal = NaN;
            obj.results.yPosThermal = NaN;
            
            
            obj.results.xRadiusBEC = NaN;
            obj.results.yRadiusBEC = NaN;
            
            
            
            obj.results.xRadiusThermal = NaN;
            obj.results.yRadiusThermal = NaN;

            
            obj.results.psdX = NaN;
            obj.results.psdY = NaN;
            
            obj.results.Intensity = NaN;
            
            obj.results.NphPulse = NaN;
            obj.results.NphAbs   = NaN;
            
            %Large number to be sure it is in a sorting.
%             obj.results.midPointsError = 1e+10;
            
            obj.results.BECSSE     = NaN;
            obj.results.thermalSSE = NaN;
            
            obj.results.fitFail = obj.fitFail;
            
        end
        
        
    end
    
%% STATIC METHODS
    methods (Static)
        function ImageSpecification = createDefaultImageSpecification
            % ImageSpecification = createDefaultImageSpecification
            % 	Create structure with required inputs to the UltracoldImage
            %   class. Unspecified inputs must be filled in before calling
            %   UltracoldImage(ImageSpecification), except RoI
            ImageSpecification = struct('date',[], ...
                'run',[],...
                'edgePixels', [20    20    20    20],...
                'fitmodel', [],...
                'Roi', [], ...
                'becRoiMethod','1D calculation',...
                'centerBoundness','Fitted',...
                'fitOffset',false,...
                'LatticeParameterLoading','Off',...
                'rot', 0,...
                'parameters',[],...
                'raw',[]);
        end
    end
    
end

