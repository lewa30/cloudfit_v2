function LatticeParameters(obj,Time,run)
    
        import Calibrations.getCalibrationParameters
        LatticeCalibParameters = getCalibrationParameters(Time, run);
         
        % Calibrations
        obj.parameters.cameraCalib = LatticeCalibParameters.cameraCalibration;
        imageDirectory = getImageDir( Time );
        % Extract probe duration and time of flight
        try
            obj.parameters.detectionTime = extractScannedValueForScript(imageDirectory,run,'Absorption pulse duration')*1e6; %due to spelling error we corrected, this might give an error of dates before november 2018
        catch
            obj.parameters.detectionTime = extractScannedValueForScript(imageDirectory,run,'Absorbtion pulse duration')*1e6;
        end
        obj.parameters.ToF = extractScannedValueForScript(imageDirectory,run,'TimeOfFlight');
         
        obj.parameters.omegaX = 2*pi*LatticeCalibParameters.fz;
        obj.parameters.omegaY = 2*pi*LatticeCalibParameters.fy;
        obj.parameters.omegaZ = 2*pi*LatticeCalibParameters.fx;
        %
        obj.parameters.pixelSize = LatticeCalibParameters.pixelSize;
        %
         
         
end