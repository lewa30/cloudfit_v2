function HiResCameras(app)


selectedButton = app.HiResCameraButtonGroup.SelectedObject.Text;

switch selectedButton
    case 'longitudinal'
        app.constants.alpha = 1;
        app.constants.detectionTime = 1;
        app.constants.cameraCalib = 3.8e-18;
        app.constants.pixelSize = 2.72e-6;
        
    case 'Luca'
        app.constants.alpha = 2.75;
        app.constants.detectionTime = 1;
        app.constants.cameraCalib = 1.51e-18;
        app.constants.pixelSize = 2.254e-6;
        
    case 'vertical'
        app.constants.alpha = 2.92;
        app.constants.detectionTime = 1;
        app.constants.cameraCalib = 3.8e-18;
        app.constants.pixelSize = 2.92e-6;

    case 'Camera 4'
        app.constants.alpha = 1;
        app.constants.detectionTime = 1;
        app.constants.cameraCalib = 1;
        app.constants.pixelSize = 1;
        
end
end