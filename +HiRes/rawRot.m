function raw = rawRot(raw,rot)

raw.atoms = imrotate(raw.atoms,rot);
raw.beam = imrotate(raw.beam,rot);

raw.atomBias = imrotate(raw.atomBias,rot);
raw.beamBias = imrotate(raw.beamBias,rot);

[raw.height, raw.width] = size(raw.atoms);

end