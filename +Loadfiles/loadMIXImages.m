function img = loadMIXImages(imageFolder,runNumber)
% Author: Jeppe Thuesen
% Email: jeppethuesen@gmail.com

% THIS FUNCTION IS NOT IN USE AS completeImageLoading IS FAR MORE GENERAL.
% This is the loadfile for MIX lab. There is a fundamental difference
% between the loading sequence for MIX and Lattice, hence different
% functions.

prefix='Andor_';

               

fileAtoms=fullfile(imageFolder,[prefix, num2str(runNumber), '_A', '.tif']);
fileAtomsBias=fullfile(imageFolder,[prefix, num2str(runNumber), '_C', '.tif']);
fileBeam=fullfile(imageFolder,[prefix, num2str(runNumber), '_B', '.tif']);
fileBeamBias=fullfile(imageFolder,[prefix, num2str(runNumber), '_D', '.tif']);




img = struct('atoms',double(imread(fileAtoms,'tiff')),...
      'beam',double(imread(fileBeam,'tiff')),...
      'atomBias',double(imread(fileAtomsBias,'tiff')),...
      'beamBias',double(imread(fileBeamBias,'tiff')),...
      'width',0,...
      'height',0);

img.width = size(img.atoms,2);
img.height = size(img.atoms,1);

information  = imfinfo(fileAtoms);
img.dateData = information.FileModDate;


end
