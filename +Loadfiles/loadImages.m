function img = loadImages(directory,runNumber,pages)
% Author Mick Kristensen


% THIS FUNCTION IS NOT IN USE AS completeImageLoading IS FAR MORE GENERAL.
% Load function for Lattice
% [atoms, beam, background1, background2] = loadImages(path,runNumber,pages)
% Loads the four images in the directory specified by the run number and
% stores images and info in a stuct img
% Example:
% 	img = loadImages(directory,runNumber)
% 		Loads the 4 single-page images from run 'runNumber' in the
% 		directory 'directory, and stores them in the 'img' struct.
%
% 	img = loadImages(directory,runNumber,pages)
% 		Loads 4 pages from the multipages tiff-file specified as pages =
% 		[atoms beam atomBias beamBias]


% If the images are old style 1-page images load them normally
if nargin < 3
   fileAtoms = fullfile(directory,['X_Atome ' num2str(runNumber) '.tif']);
   fileBeam = fullfile(directory,['X_Beam ' num2str(runNumber) '.tif']);
   fileAtomsBias = fullfile(directory,['X_Backgr ' num2str(runNumber) '.tif']);
   fileBeamBias = fullfile(directory,['X_BackgrB ' num2str(runNumber) '.tif']);
   
   info  = imfinfo(fileAtoms);
   
   img = struct('atoms',double(imread(fileAtoms,'tiff')),...
      'beam',double(imread(fileBeam,'tiff')),...
      'atomBias',double(imread(fileAtomsBias,'tiff')),...
      'beamBias',double(imread(fileBeamBias,'tiff')),...
      'width',0,...
      'height',0);
else
    
    
    
   % If the images are stored as a multipage Tiff file load the specified
   % pages
  
   fileName = fullfile(directory,['X_abs#' num2str(runNumber) '.tif']);
%    fileName = fullfile(directory,['X_axis_secondary_camera_' num2str(runNumber) '.tif']);
   info = imfinfo(fileName);
   img = struct('atoms',double(imread(fileName, pages(1),'Info', info)),...
      'beam',double(imread(fileName, pages(2),'Info', info)),... %Change back to 2
      'atomBias',double(imread(fileName, pages(3),'Info', info)),...
      'beamBias',double(imread(fileName, pages(4),'Info', info)),...
      'width',0,...
      'height',0);
end


img.width = size(img.atoms,2);
img.height = size(img.atoms,1);


if isfield(info, 'DateTime')
    img.dateData = datetime(info(1).DateTime, 'InputFormat', 'yyyy:MM:dd HH:mm:ss');
else
    img.dateData = NaT;
end


end