function img = completeImageLoad(baseFileName, pageLoading, loadOrder, runNumber, date)
%author: Jeppe Thuesen
%This code is build on the other three codes in the same folder 
%(loadHiResImages, loadImages, loadMIXImages). This should make the others obsolete. 

%% let's load an image!
switch pageLoading.method
    case 'pages'
        fileName = char(baseFileName + ".tif");
        
        pages = str2double(pageLoading.pages);
        
        info = imfinfo(fileName);
        img = struct('atoms',double(imread(fileName, loadOrder(1),'Info', info)),...
            'beam',double(imread(fileName, loadOrder(2),'Info', info)),...
            'atomBias',double(imread(fileName, loadOrder(3),'Info', info)),...
            'beamBias',double(imread(fileName, loadOrder(4),'Info', info)),...
            'width',0,...
            'height',0);
        
        
    case 'suffix'
      fileName = char(baseFileName + pageLoading.pages + ".tif");
        
        img = struct('atoms',double(imread(fileName(loadOrder(1),:),'tiff')),...
      'beam',double(imread(fileName(loadOrder(2),:),'tiff')),...
      'atomBias',double(imread(fileName(loadOrder(3),:),'tiff')),...
      'beamBias',double(imread(fileName(loadOrder(4),:),'tiff')),...
      'width',0,...
      'height',0);
  
  info = imfinfo(fileName(1,:));
        
end


img.width = size(img.atoms,2);
img.height = size(img.atoms,1);


img.dateData = info.FileModDate;

img.run = runNumber;
img.date = date;




end