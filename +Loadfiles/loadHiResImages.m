function img = loadHiResImages(imageFolder,runNumber,prefix)
% Author: Jeppe Thuesen
% Email: jeppethuesen@gmail.com

% THIS FUNCTION IS NOT IN USE AS completeImageLoading IS FAR MORE GENERAL.

prefix = [prefix '_'];

numberString = num2str(runNumber,'%04d');

fileAtoms=fullfile(imageFolder,[prefix, numberString, '_A', '.tif']);
fileBeam=fullfile(imageFolder,[prefix, numberString, '_B', '.tif']);
fileRef = fullfile(imageFolder,[prefix, numberString, '_C', '.tif']);


img = struct('atoms',double(imread(fileAtoms,'tif')),...
      'beam',double(imread(fileBeam,'tif')),...
      'atomBias',double(imread(fileRef,'tif')),...
      'beamBias',double(imread(fileRef,'tif')),...
      'width',0,...
      'height',0);


information  = imfinfo(fileAtoms);
img.dateData = information.FileModDate;

img.width = size(img.atoms,2);
img.height = size(img.atoms,1);
end


 
