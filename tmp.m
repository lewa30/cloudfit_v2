function parameters = tmp(req, opt, varargin)
parser = inputParser;

parser.addRequired('req')
parser.addOptional('opt', 'opt')

parser.addParameter('par1', 'par1', @islogical)

parser.parse(req, opt, varargin{:});

parameters = parser.Results;
end