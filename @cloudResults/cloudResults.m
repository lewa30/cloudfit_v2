classdef cloudResults < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Access = public)
        % Masks
        runNumber
        NSum
        NBECSum
        Nthermal
        NBEC
        Tc
        TX
        TY
        TXTc
        TYTc
        xPos
        yPos
        xRadiusBEC
        yRadiusBEC
        xWidthThermal
        yWidthThermal
        psdX
        psdY
        BECSSE
        thermalSSE
        fitFailed
        
    end
    properties (Access = private)
        
        %Inputs
        constants = struct;
        od        = NaN;
        Roi       = struct;
        ThermalOn = false;
        BecOn     = false;
        fitOffset = false;
        X = NaN;
        Y = NaN;
        
        
        thermalModel
        BECModel
        
        
        %Calculated constants
        od2atomNumber
        widthSquared2temperatureX
        widthSquared2temperatureY
        
        %Nature constants
        kB                  = 1.380648520000000e-23;
        hbar                = 1.0545718e-34;
        
        
    end
    
    
    methods
        
        function obj = cloudResults(inputStruct)
            obj = obj.parseInputStructure( inputStruct );
            
            obj.CalculateConstants
            obj.CalculateResults
        end
        
        
        function obj = parseInputStructure(obj, inputStruct )
            % Setup inputparser
            parser = inputParser;
            parser.FunctionName = 'results';
            
            % Setup validation functions
            chkScalar = @(x) validateattributes(x, {'numeric'}, {'finite', 'scalar'});
            chkVector = @(x,size) validateattributes(x, {'numeric'}, {'finite', 'numel', size});
            
            % Add parameters
            parser.addParameter('constants', obj.constants, @isstruct)
            parser.addParameter('runNumber', obj.runNumber, @isnumeric)
            parser.addParameter('od', obj.od)
            parser.addParameter('Roi',obj.Roi)
            parser.addParameter('ThermalOn', obj.ThermalOn, @islogical)
            parser.addParameter('BecOn', obj.BecOn, @islogical)
            parser.addParameter('fitFailed', obj.fitFailed, @isnumeric)
            parser.addParameter('fitOffset', obj.fitOffset, @islogical)
            parser.addParameter('thermalModel',obj.thermalModel)
            parser.addParameter('BECModel', obj.BECModel)
            parser.addParameter('X', obj.X)
            parser.addParameter('Y', obj.Y)
            
            
            parser.parse( inputStruct );
            
            % Assign parser result to object properties
            parameterNames = fieldnames(parser.Results);
            nfileds        = numel(parameterNames);
            for ifield = 1:nfileds
                obj.(parameterNames{ifield}) = parser.Results.(parameterNames{ifield});
            end
        end
        
        
        function  CalculateConstants(obj)
            obj.od2atomNumber = obj.constants.pixelSize^2/obj.constants.crosssection;
            
            obj.widthSquared2temperatureX = obj.constants.mass/obj.kB/...
                (1/obj.constants.omegaX^2 + obj.constants.ToF^2);
            
            obj.widthSquared2temperatureY = obj.constants.mass/obj.kB/...
                (1/obj.constants.omegaY^2 + obj.constants.ToF^2);
            obj.Tc = obj.hbar*(obj.constants.omegaX*obj.constants.omegaY*obj.constants.omegaZ)^(1/3)...
                *obj.NSum^(1/3)/(zeta(3)^(1/3)*obj.kB);
        end
        
        function CalculateResults(obj)
            if obj.fitFailed == 1
                obj.failedFit
            end
            
            obj.NSum        = sum(obj.od(obj.Roi.cloud))*obj.od2atomNumber;
            
            if obj.ThermalOn == false && obj.BecOn == false
                return
            end
            
            %If there is an offset in the picture, this can have a impact
            %on NSum, so if that is the case, it must be subtracted
            if obj.fitOffset == 1 && obj.ThermalOn == 1
                obj.NSum = obj.NSum-sum(sum(obj.Roi.cloud))...
                    *obj.thermalModel.Coefficients{'offset','Estimate'}*obj.od2atomNumber;
            end
            %
            if obj.NSum < 0
                
                obj.fitFailed = 1;
                obj.failedFit
                
                disp('Error: NSum is negative')
                
                return
            end
            
            if obj.ThermalOn == 1
                if obj.BecOn == 1
                    obj.NBECSum ...
                        = sum(obj.od(obj.Roi.bec) - obj.thermalModel.feval(obj.X(obj.Roi.bec), obj.Y(obj.Roi.bec)))*obj.od2atomNumber;
                else
                    obj.NBECSum = NaN;
                end
                
                
                
                obj.Nthermal ...
                    = obj.thermalModel.Coefficients{'Volume', 'Estimate'}*obj.od2atomNumber;
            else
                obj.NBECSum = sum(obj.od(obj.Roi.bec))*obj.od2atomNumber;
                obj.Nthermal = NaN;
            end
            
            if obj.BecOn == 1
                obj.NBEC    = obj.BECModel.Coefficients{'Volume', 'Estimate'}*obj.od2atomNumber;
            else
                obj.NBEC = NaN;
            end
            
            %% Temperature results
            obj.Tc = obj.hbar*(obj.constants.omegaX*obj.constants.omegaY*obj.constants.omegaZ)^(1/3)...
                *obj.NSum^(1/3)/(zeta(3)^(1/3)*obj.kB);
            
            if obj.ThermalOn == 1
                obj.TX = (obj.thermalModel.Coefficients{'xWidth', 'Estimate'})^2 ...
                    *obj.constants.pixelSize^2*obj.widthSquared2temperatureX;
                
                obj.TY = (obj.thermalModel.Coefficients{'yWidth', 'Estimate'})^2 ...
                    *obj.constants.pixelSize^2*obj.widthSquared2temperatureY;
                
                
                
                obj.TXTc = obj.TX/obj.Tc;
                obj.TYTc = obj.TY/obj.Tc;
                
                
                
            else
                
                obj.TX   = NaN;
                obj.TY   = NaN;
                obj.TXTc = NaN;
                obj.TYTc = NaN;
            end
            
            roundedY = round(obj.Roi.centerY);
            roundedX = round(obj.Roi.centerX);
            
            %So if we have a BEC we want the center of the atomic cloud to be displayed
            %as the center of the BEC. If there is no BEC we want the center of the
            %thermal cloud as our center.
            
                    if size(obj.BECModel.Coefficients{:,1},1) == 5
                        obj.xPos = obj.BECModel.Coefficients{'xCenter','Estimate'};
                        obj.yPos = obj.BECModel.Coefficients{'yCenter','Estimate'};
                    elseif size(obj.thermalModel.Coefficients{:,1},1) == 5
                        obj.xPos = obj.thermalModel.Coefficients{'xCenter','Estimate'};
                        obj.yPos = obj.thermalModel.Coefficients{'yCenter','Estimate'};
                    else
                        obj.xPos = obj.Roi.centerX;
                        obj.yPos = obj.Roi.centerY;
                    end
               
           
                    
                    
            
            
            if obj.BecOn == 1
                obj.xRadiusBEC = obj.BECModel.Coefficients{'xRadius','Estimate'};
                obj.yRadiusBEC = obj.BECModel.Coefficients{'yRadius','Estimate'};
                
            else
                obj.xRadiusBEC = NaN;
                obj.yRadiusBEC = NaN;
            end
            
            if obj.ThermalOn == 1
                obj.xWidthThermal = obj.thermalModel.Coefficients{'xWidth','Estimate'};
                obj.yWidthThermal = obj.thermalModel.Coefficients{'yWidth','Estimate'};
                
            else
                obj.xWidthThermal = NaN;
                obj.yWidthThermal = NaN;
            end
            
            if obj.ThermalOn == 1 && obj.BecOn == 0
                obj.psdX = obj.Nthermal*obj.hbar^3*...
                    obj.constants.omegaX*obj.constants.omegaY*obj.constants.omegaZ...
                    /(obj.kB*obj.TX)^3;
                
                obj.psdY = obj.Nthermal*obj.hbar^3*...
                    obj.constants.omegaX*obj.constants.omegaY*obj.constants.omegaZ...
                    /(obj.kB*obj.TY)^3;
                if obj.hot == 0
                    obj.psdX = obj.psdX/zeta(3);
                    obj.psdY = obj.psdY/zeta(3);
                end
                
                %                 obj.PSD = 1/((2*pi)^(3/2)*...
                %                     obj.thermalModel.Coefficients{'xWidth', 'Estimate'}*...
                %                     obj.thermalModel.Coefficients{'yWidth', 'Estimate'}*...
                %                     obj.thermalModel.Coefficients{'xWidth', 'Estimate'});
            else
                obj.psdX = NaN;
                obj.psdY = NaN;
            end
            
            
            sides = 3; % Changeable. This determines how many values to
            %look at, in order to determine the error value.
            peakRaw = obj.od(roundedY,roundedX-sides:roundedX+sides);
            
            if obj.BecOn == 1
                
                if obj.BECModel.Coefficients{'Volume','Estimate'}<0
                    %If the Volume is negative, the fit have failed
                    %therefore an extremely large number is used to say
                    %that it failed.
%                     obj.midPointsError = 1e+100;
                    
                else %If the volume seems okay
                    if obj.ThermalOn ==1
                        
                        peakFit = obj.thermalModel.feval(roundedX-sides:roundedX+sides,roundedY)...
                            +obj.BECModel.feval(roundedX-sides:roundedX+sides,roundedY);
                        
                    else
                        peakFit = obj.BECModel.feval(roundedX-sides:roundedX+sides,roundedY);
                        
                    end
%                     obj.midPointsError = mean(((peakRaw-peakFit)./max(peakRaw)).^2);
                end
            else %obj.BecOn == 0 && obj.ThermalOn == 1
                peakFit = obj.thermalModel.feval(roundedX-sides:roundedX+sides,roundedY);
%                 obj.midPointsError = mean(((peakRaw-peakFit)./max(peakRaw)).^2);
            end
            
            if obj.BecOn == 1
                obj.BECSSE     = obj.BECModel.SSE;
            else
                obj.BECSSE = NaN;
            end
            if obj.ThermalOn == 1
                obj.thermalSSE = obj.thermalModel.SSE;
            else
                obj.thermalSSE = NaN;
            end
            
            
        end
        
        
        function failedFit(obj)
            
            % Calculate summing results
            
            obj.NSum        = sum(obj.od(obj.Roi.cloud))*obj.od2atomNumber;
            
            
            % Sets every output to NaN as to avoid errors or numbers that
            % are certainly off.
            obj.NBECSum  = NaN;
            obj.Nthermal = NaN;
            obj.NBEC     = NaN;
            
            
            
            obj.TX   = NaN;
            obj.TY   = NaN;
            obj.TXTc = NaN;
            obj.TYTc = NaN;
            
            obj.xPos = NaN;
            obj.yPos = NaN;
            
            obj.xRadius = NaN;
            obj.yRadius = NaN;
            
            obj.psdX = NaN;
            obj.psdY = NaN;
            %Large number to be sure it is in a sorting.
            obj.midPointsError = 1e+100;
            
            obj.BECSSE     = NaN;
            obj.thermalSSE = NaN;
            
        end
        
        
        
        
        
        
        
    end
    
end