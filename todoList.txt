  TODO:
Before release:
 

After release:
    - Micks new thermal fitting routine
    - Batch evaluation ( load old file and reevaluate all data from that
    file)
        THIS is now made avaliable though lattice_hacking.m by using
        Lattice's dataset format.
    
    - Make dual imaging avaliable (already possible by opening matlab two
    times and then running the app in both)
    

    - Find a way to have the same size of the GUI for all computers (done by manually making the app smaller,
	however it might be beneficial to find an automated way to do it)
    - Class structure for the results. (this has been attempted in \@cloudResults\cloudResults.m, but it does not
	couple corretly to the rest of the code in the app.evaluationMain function)
    - Maybe make a standard SkyFit evaluation script?
    
    - Remove imaginary numbes from output file

    - Make a loading mode, where an old file is loaded and the runs here
    are the ones to evaluate again (then missing and previously removed
    pictures will not be reevaluated, can be done with the "hack" method in lattice_hacking.m).

MIX Ideas
- Lav tredje mulighed for center boundness: Fix BEC center ud fra 2D gaussisk fit





Nice wishlist of nice ideas that will be nice to have. Which is nice

    - Make it possible to evaluate data on different days without actively
    changing anything. 
        -As of yet this can be done by making a script that couples to
        UltracoldImage. It can also be done by using the app once and then
        "hacking" the GUI (method can be seen in Lattice_hacking.m).
  

%}