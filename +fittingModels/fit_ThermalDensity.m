function thermalFit = fit_ThermalDensity(od, roi, X, Y, peak,fitOffset, varargin)
%% thermalFit = fit_BoseEnhancedGaussian(od, roi, varargin)
%	Fit Bose-enhanced thermal distribution
%
%	26 February, 2018, Mick Kristensen mick@phys.au.dk

% Parse input arguments
parser = parseArguments(od, roi,peak,fitOffset, varargin{:});
inputs  = parser.Results;

% Setup table for fitting
% [X, Y] = meshgrid(1:size(inputs.od,2),1:size(inputs.od,1));
tbl = table(X(roi), Y(roi), inputs.od(roi), 'VariableNames', {'X', 'Y', 'od'});



% Parse starting guess
startingGuess = parseStartingGuess(tbl, inputs,peak);

% Setup model
[model, coefficientNames] = selectFittingmodel(inputs);

ValidCoefficients = ["Volume", "xCenter", "xWidth", "yCenter", "yWidth", "offset", "fugacity"];

if any(~ismember(coefficientNames, ValidCoefficients))
   error('fit_ThermalDensity:UnknownFittingCoefficients', ...
      'The desired coefficients were unknown, make sure the model only contains \n\t%s', ...
      sprintf('%s ', ValidCoefficients))
end

thermalFit = fitnlm(tbl, model, startingGuess, 'options', inputs.options,...
   'CoefficientNames', coefficientNames);


if any(thermalFit.Coefficients.Estimate < 0 ) == true
   % It is not physically possible for any of the parameters fitted to here to be
   % negative. But as the widths are squared a negative sign does not change the
   % fit, however as these numbers are divided with the volume in the front
   % factor, then this would force the volume to be negative while the fit seems
   % perfectly fine. Therefor another fit is done, but with the positive values
   % of the previous fit as start guesses, thus hopefully forcing the parameters
   % to be positive.
   
   newStartingGuess = abs(thermalFit.Coefficients.Estimate);
   
   thermalFit = fitnlm(tbl, model, newStartingGuess, 'options', inputs.options,...
   'CoefficientNames', coefficientNames);
   
   
end

               %
% % Create result table
% thermalResult = table('RowNames', ValidCoefficients);
% thermalResult.Estimate = zeros(length(ValidCoefficients), 1);
% 
% % Pass the fitted coefficients
% thermalResult{coefficientNames, 'Estimate'} = thermalFit.Coefficients{coefficientNames, 'Estimate'};
% 



end
function parser = parseArguments(od, roi,peak,fitOffset, varargin)
	parser = inputParser;
	parser.FunctionName = 'fit_ThermalDensity';
	
	% Required inputs
	parser.addRequired('od', @isnumeric)
	parser.addRequired('roi', @(x) validateattributes(x, {'logical'}, {'numel', numel(od)}, 2))
	
	% Optional parameters with default values
	parser.addParameter('startingGuess', []) % We treat parsing of the starting guess later
	parser.addParameter('fitOffset', fitOffset, @islogical)
	parser.addParameter('fitFugacity', false, @islogical)
	parser.addParameter('fugacity', 1, @(x) validateattributes(x, {'numeric'}, {'scalar','>=' 0, '<=', 1}))
	parser.addParameter('fitBoseEnhanced', true, @islogical)
	parser.addParameter('options', statset, @isstruct)
   parser.addParameter('peak', peak, @isnumeric)
	
	parser.parse(od, roi, varargin{:})

end
function startingGuess = parseStartingGuess(tbl, inputs,peak)
% Make an automatic guess for the starting parameters if the starting guess
% is empty
if isempty(inputs.startingGuess)
	% Calculate weights
	w = tbl.od - min(tbl.od) + eps;
	w = w/max(w);
	
	% Calculate weighted centroid
	xCenter = sum(tbl.X.*w)/sum(w);
	yCenter = sum(tbl.Y.*w)/sum(w);

	% Average azimuthally and fit a gaussian distribution
	dist = sqrt((tbl.X - xCenter).^2 + (tbl.Y - yCenter).^2);
	dens = tbl.od;
    
	fo = fit([dist; -dist], [dens; dens], ...
        'a1*exp(-((x-b1)/c1)^2)+d1',...
        'StartPoint', [2*max(max(dens)), 0, 50, 0], ...
        'Lower', [0, -Inf, 0, -Inf], ...
        'Upper', [peak, Inf, Inf, Inf],...
        'Weights', 1./[dist; dist].^2);
	
	xWidth  = fo.c1/sqrt(2);
	yWidth  = fo.c1/sqrt(2);
	
	volume = fo.a1*2*pi*xWidth*yWidth;
	
	offset  = 0;
	fugacity = inputs.fugacity;
elseif isstruct(inputs.startingGuess)
	% Parse starting guess input as struct
	volume = inputs.startingGuess.amplitude;
	xCenter	  = inputs.startingGuess.xCenter;
	yCenter   = inputs.startingGuess.yCenter;
	xWidth   = inputs.startingGuess.xWidth;
	yWidth   = inputs.startingGuess.yWidth;
	if isfield(inputs.startingGuess, 'offset')
		offset = inputs.startingGuess.offset;
	end
	if isfield(inputs.startingGuess, 'fugacity')
		fugacity = inputs.startingGuess.fugacity;
	end
elseif isdouble(inputs.startingGuess)
	volume = inputs.startingGuess(1);
	xCenter	  = inputs.startingGuess(1);
	yCenter   = inputs.startingGuess(1);
	xWidth    = inputs.startingGuess(1);
	yWidth    = inputs.startingGuess(1);
	if inputs.fitOffset
		offset = inputs.startingGuess(6);
	end
	if inputs.fitFugacity
		fugacity = inputs.startingGuess(end);
	end
else
	error('Could not parse starting guess input')
end

if inputs.fitFugacity && inputs.fitOffset
	startingGuess = [volume, xCenter, xWidth, yCenter, yWidth, offset, fugacity];
elseif inputs.fitFugacity
	startingGuess = [volume, xCenter, xWidth, yCenter, yWidth, fugacity];
elseif inputs.fitOffset
	startingGuess = [volume, xCenter, xWidth, yCenter, yWidth, offset];
else
	startingGuess = [volume, xCenter, xWidth, yCenter, yWidth];
end
	
end
function [model, coefficientNames] = selectFittingmodel(inputs)
if inputs.fitBoseEnhanced
	g2 = createPolylog2Lookup;
	if inputs.fitFugacity && inputs.fitOffset
		error('Fugacity fit not implemented yet')
	elseif inputs.fitFugacity
		error('Fugacity fit not implemented yet')
	elseif inputs.fitOffset
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(5)*zeta(3))*g2(exp(-(tbl(:,1) - p(2)).^2/(2*p(3)^2) - (tbl(:,2) - p(4)).^2/(2*p(5)^2))) + p(6);
		coefficientNames = {'Volume', 'xCenter', 'xWidth', 'yCenter', 'yWidth', 'offset'};
	else
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(5)*zeta(3))*g2(exp(-(tbl(:,1) - p(2)).^2/(2*p(3)^2) - (tbl(:,2) - p(4)).^2/(2*p(5)^2)));
		coefficientNames = {'Volume', 'xCenter', 'xWidth', 'yCenter', 'yWidth'};
	end
else
	if inputs.fitOffset
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(5))*(exp(-(tbl(:,1) - p(2)).^2/(2*p(3)^2) - (tbl(:,2) - p(4)).^2/(2*p(5)^2)) + p(6));
		coefficientNames = {'Volume', 'xCenter', 'xWidth', 'yCenter', 'yWidth', 'offset'};
	else
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(5))*(exp(-(tbl(:,1) - p(2)).^2/(2*p(3)^2) - (tbl(:,2) - p(4)).^2/(2*p(5)^2)));
		coefficientNames = {'Volume', 'xCenter', 'xWidth', 'yCenter', 'yWidth'};
	end
end
end