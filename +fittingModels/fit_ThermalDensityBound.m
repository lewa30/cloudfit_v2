function thermalFit = fit_ThermalDensityBound(od, roi, X, Y, peak,centerXGuess,centerYGuess,fitOffset, varargin)
%% thermalFit = fit_BoseEnhancedGaussian(od, roi, varargin)
%	Fit Bose-enhanced thermal distribution
%
%	26 February, 2018, Mick Kristensen mick@phys.au.dk
%   Modifications made by Jeppe Thuesen jeppethuesen@gmail.com

% Parse input arguments
parser = parseArguments(od, roi,peak,centerXGuess, centerYGuess,fitOffset, varargin{:});
inputs  = parser.Results;

% Setup table for fitting
% [X, Y] = meshgrid(1:size(inputs.od,2),1:size(inputs.od,1));
tbl = table(X(roi), Y(roi), inputs.od(roi), 'VariableNames', {'X', 'Y', 'od'});



% Parse starting guess
startingGuess = parseStartingGuess(tbl, inputs,peak);

% Setup model
[model, coefficientNames] = selectFittingmodel(inputs);



thermalFit = fitnlm(tbl, model, startingGuess, 'options', inputs.options,...
						'CoefficientNames', coefficientNames);
               
               
               
if any(thermalFit.Coefficients.Estimate < 0 ) == true
   % It is not physically possible for any of the parameters fitted to here to be
   % negative. But as the widths are squared a negative sign does not change the
   % fit, however as these numbers are divided with the volume in the front
   % factor, then this would force the volume to be negative while the fit seems
   % perfectly fine. Therefor another fit is done, but with the positive values
   % of the previous fit as start guesses, thus hopefully forcing the parameters
   % to be positive.
   
   newStartingGuess = abs(thermalFit.Coefficients.Estimate);
   
   thermalFit = fitnlm(tbl, model, newStartingGuess, 'options', inputs.options,...
   'CoefficientNames', coefficientNames);
   
   
end


end
function parser = parseArguments(od, roi,peak,centerXGuess, centerYGuess, fitOffset, varargin)
	parser = inputParser;
	parser.FunctionName = 'fit_ThermalDensity';
	
	% Required inputs
	parser.addRequired('od', @isnumeric)
	parser.addRequired('roi', @(x) validateattributes(x, {'logical'}, {'numel', numel(od)}, 2))
	
	% Optional parameters with default values
	parser.addParameter('startingGuess', []) % We treat parsing of the starting guess later
	parser.addParameter('fitOffset', fitOffset, @islogical)
	parser.addParameter('fitFugacity', false, @islogical)
	parser.addParameter('fugacity', 1, @(x) validateattributes(x, {'numeric'}, {'scalar','>=' 0, '<=', 1}))
	parser.addParameter('fitBoseEnhanced', true, @islogical)
	parser.addParameter('options', statset, @isstruct)
    parser.addParameter('peak', peak, @isnumeric)
	parser.addParameter('centerXGuess', centerXGuess, @isnumeric)
    parser.addParameter('centerYGuess', centerYGuess, @isnumeric)
    
	parser.parse(od, roi, varargin{:})

end
function startingGuess = parseStartingGuess(tbl, inputs,peak)
% Make an automatic guess for the starting parameters if the starting guess
% is empty
if isempty(inputs.startingGuess)
	% Calculate weights
	w = tbl.od - min(tbl.od) + eps;
	w = w/max(w);
	
    % Use Roi calculated center as center Guess
    xCenter = inputs.centerXGuess;
    yCenter = inputs.centerYGuess;
    
    
	% Calculate weighted centroid
% 	xCenter = sum(tbl.X.*w)/sum(w);
% 	yCenter = sum(tbl.Y.*w)/sum(w);

	% Average azimuthally and fit a gaussian distribution
	dist = sqrt((tbl.X - xCenter).^2 + (tbl.Y - yCenter).^2);
	dens = tbl.od;
    
	fo = fit([dist; -dist], [dens; dens], ...
        'a1*exp(-((x-b1)/c1)^2)+d1',...
        'StartPoint', [2*max(max(dens)), 0, 50, 0], ...
        'Lower', [0, -Inf, 0, -Inf], ...
        'Upper', [peak, Inf, Inf, Inf],...
        'Weights', 1./[dist; dist].^2);
	
	xWidth  = fo.c1/sqrt(2);
	yWidth  = fo.c1/sqrt(2);
	
	volume = fo.a1*2*pi*xWidth*yWidth;
	
	offset  = 0;
	fugacity = inputs.fugacity;
elseif isstruct(inputs.startingGuess)
	% Parse starting guess input as struct
	volume = inputs.startingGuess.amplitude;
% 	xCenter	  = inputs.startingGuess.xCenter;
% 	yCenter   = inputs.startingGuess.yCenter;
	xWidth   = inputs.startingGuess.xWidth;
	yWidth   = inputs.startingGuess.yWidth;
	if isfield(inputs.startingGuess, 'offset')
		offset = inputs.startingGuess.offset;
	end
	if isfield(inputs.startingGuess, 'fugacity')
		fugacity = inputs.startingGuess.fugacity;
	end
elseif isdouble(inputs.startingGuess)
	volume = inputs.startingGuess(1);
% 	xCenter	  = inputs.startingGuess(1);
% 	yCenter   = inputs.startingGuess(1);
	xWidth    = inputs.startingGuess(1);
	yWidth    = inputs.startingGuess(1);
	if inputs.fitOffset
		offset = inputs.startingGuess(6);
	end
	if inputs.fitFugacity
		fugacity = inputs.startingGuess(end);
	end
else
	error('Could not parse starting guess input')
end

if inputs.fitFugacity && inputs.fitOffset
	startingGuess = [volume, xWidth, yWidth, offset, fugacity];
elseif inputs.fitFugacity
	startingGuess = [volume, xWidth, yWidth, fugacity];
elseif inputs.fitOffset
	startingGuess = [volume, xWidth, yWidth, offset];
else
	startingGuess = [volume, xWidth, yWidth];
end
	
end
function [model, coefficientNames] = selectFittingmodel(inputs)
        xCenter = inputs.centerXGuess;
        yCenter = inputs.centerYGuess;
        
        
if inputs.fitBoseEnhanced
	g2 = createPolylog2Lookup;
	if inputs.fitFugacity && inputs.fitOffset
		error('Fugacity fit not implemented yet')
	elseif inputs.fitFugacity
		error('Fugacity fit not implemented yet')
	elseif inputs.fitOffset   
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(2)*zeta(3))*g2(exp(-(tbl(:,1) - xCenter).^2/(2*p(2)^2) - (tbl(:,2) - yCenter).^2/(2*p(3)^2))) + p(4);
		coefficientNames = {'Volume', 'xWidth', 'yWidth', 'offset'};
    else
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(2)*zeta(3))*g2(exp(-(tbl(:,1) - xCenter).^2/(2*p(2)^2) - (tbl(:,2) - yCenter).^2/(2*p(3)^2)));
		coefficientNames = {'Volume', 'xWidth', 'yWidth'};
	end
else
	if inputs.fitOffset
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(2))*(exp(-(tbl(:,1) - xCenter).^2/(2*p(2)^2) - (tbl(:,2) - yCenter).^2/(2*p(3)^2)) + p(4));
		coefficientNames = {'Volume', 'xWidth', 'yWidth', 'offset'};
	else
		model = @(p,tbl) p(1)/(2*pi*p(3)*p(2))*(exp(-(tbl(:,1) - xCenter).^2/(2*p(2)^2) - (tbl(:,2) - yCenter).^2/(2*p(3)^2)));
		coefficientNames = {'Volume', 'xWidth', 'yWidth'};
	end
end
end