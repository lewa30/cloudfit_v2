function cloud = fitBimodal1D(OD, RoI, maxOD, fitType)


% Calculate Center of mass of image to get estimates of cloud center and
% width
% [cropOD, xArrray, yArray] = utility.cropImage(OD, RoI.signal);
% [centerX, centerY, sizeX, sizeY] = utility.centerofmass(cropOD);
% [cx,cy,sx,sy] = estimateCenterAndWidth(cropOD);
cropOD = OD.*ROI

% Fitting model
% TF_1D =		 @(A,x0,R,X) A*max(0,(1 - ((X-x0)/R).^2 )).^2;
TF_1D =		 @(A,x0,R,X) A*max(0,(1 - ((X-x0)/R).^2 )).^(3/2);
thermal_1D = @(A,x0,w,X) A*exp( -(X-x0).^2/(2*w^2) );
switch fitType 
    case 'bimodal'
        model =	 @(p,X) TF_1D(p(1),p(2),p(3),X) + thermal_1D(p(4),p(2),p(3) + p(5),X) + p(6);
    case 'thermal'
        model =	 @(p,X) thermal_1D(p(4),p(2),p(5),X) + p(6);
    case 'bec'
        model =	 @(p,X) TF_1D(p(1),p(2),p(3),X)  + p(6);
        sizeX = sizeX*2;
        sizeY = sizeY*2;
end
% p1: Amplitude BEC
% p2: Center Cloud
% p3: TF radius BEC
% p4: Amplitude Thermal
% p3 + p5: Width Thermal, constrained to be larger than TF radius
% p6: Offset

%% Fit X-direction
% Fit middle slice of cloud
yy = cropOD(round(centerY),:);
xx = xArrray;
peakOD = max(yy);

xx = xx(:);
yy = yy(:);


% Exclude saturated OD from fit
satIDX = yy < maxOD;

% Initial Guess and bounds
p0 = [peakOD/2 centerX+xx(1) sizeX peakOD/2 sizeX 0];
lb = [0, p0(2)-sizeX, 0, 0, 0, -1];
ub = [2*peakOD, p0(2)+sizeX, 4*sizeX, 2*peakOD, 4*sizeX, +1];

options = optimset('MaxFunEvals',10000000000,'TolFun',1e-8,'TolX',1e-8,'Display','off');
[pX,~,~,exitflag,~,~,~] = lsqcurvefit(model,p0,xx(satIDX),yy(satIDX),lb,ub,options);
if exitflag < 0
	error('fit_BIMODAL_1D:FitNotConverged','Fit algorithm terminated without converging')
elseif exitflag == 0
	error('fit_BIMODAL_1D:FitIterExceeded','Max function evaluations exceeded')
end

% When no BEC is present, set the BEC parameters to 0, similar for thermal;
switch fitType 
    case 'thermal'
        pX([1 3]) = 0;
    case 'bec'
        pX(4) = 0;
        pX(5) = -pX(3);
end

%% Fit Y-direction
% Fit middle slice of cloud
yy = cropOD(:,round(centerX));
xx = yArray;
peakOD = max(yy);

xx = xx(:);
yy = yy(:);

% Exclude saturated OD from fit
satIDX = yy < maxOD;

% Initial Guess and bounds
p0 = [peakOD/2 centerY+xx(1) sizeY peakOD/2 sizeY 0];
lb = [0, p0(2)-sizeX, 0, 0, 0, -1];
ub = [2*peakOD, p0(2)+sizeX, 4*sizeY, 2*peakOD, 4*sizeY, +1];

options = optimset('MaxFunEvals',10000000000,'TolFun',1e-8,'TolX',1e-8,'Display','off');
[pY,~,~,exitflag,~,~,~] = lsqcurvefit(model,p0,xx(satIDX),yy(satIDX),lb,ub,options);
if exitflag < 0
	error('fit_BIMODAL_1D:FitNotConverged','Fit algorithm terminated without converging')
elseif exitflag == 0
	error('fit_BIMODAL_1D:FitIterExceeded','Max function evaluations exceeded')
end

% When no BEC is present, set the BEC parameters to 0;
switch fitType 
    case 'thermal'
        pY([1 3]) = 0;
    case 'bec'
        pY(4) = 0;
        pY(5) = -pY(3);
end


cloud = struct('BECamplitude',mean([pX(1) pY(1)]),...
	'thermalAmplitude',mean([pX(4) pY(4)]),...
	'rBECx',pX(3),...
	'rBECy',pY(3),...
	'wThermalX',pX(3)+pX(5),...
	'wThermalY',pY(3)+pY(5),...
	'centerX',pX(2),...
	'centerY',pY(2),...
	'offset',mean([pX(6) pY(6)]));
end