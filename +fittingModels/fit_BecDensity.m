function becFit = fit_BecDensity(od, roi, thermalMat, X, Y, fitOffset, varargin)
%% thermalFit = fit_BoseEnhancedGaussian(od, roi, varargin)
%	Fit Bose-enhanced thermal distribution
%
%	26 February, 2018, Mick Kristensen mick@phys.au.dk

% Parse input arguments
parser = parseArguments(od, roi, thermalMat, X, Y,fitOffset, varargin{:});
inputs  = parser.Results;

% Subtract thermal density from image to fit BEC
od = inputs.od(roi) - inputs.thermalMat;

% Setup table for fitting
tbl = table(inputs.X(roi), inputs.Y(roi), od, 'VariableNames', {'X', 'Y', 'od'});

% Parse starting guess
startingGuess = parseStartingGuess(tbl, inputs);

% Setup model
[model, coefficientNames] = selectFittingmodel(inputs);

becFit = fitnlm(tbl, model, startingGuess, 'options', inputs.options,...
						'CoefficientNames', coefficientNames);

end
function parser = parseArguments(od, roi, thermalMat, X, Y, fitOffset, varargin)
	parser = inputParser;
	parser.FunctionName = 'fit_BecDensity';
    chkMatrix = @(x,size) validateattributes(x, {'numeric'}, {'finite', 'size', size});
	
	% Required inputs
	parser.addRequired('od', @isnumeric)
	parser.addRequired('roi', @(x) validateattributes(x, {'logical'}, {'numel', numel(od)}, 2))
	parser.addRequired('thermalMat',@(x) chkMatrix(x,size(X(roi))));
	parser.addRequired('X', @(x) validateattributes(x, {'numeric'}, {'numel', numel(od)}, 4));
	parser.addRequired('Y', @(x) validateattributes(x, {'numeric'}, {'numel', numel(od)}, 5));
	
	% Optional parameters with default values
	parser.addParameter('startingGuess', []) % We treat parsing of the starting guess later
	parser.addParameter('fitOffset', fitOffset, @islogical)
	parser.addParameter('options', statset, @isstruct)
	
	parser.parse(od, roi, thermalMat, X, Y, varargin{:})

end
function startingGuess = parseStartingGuess(tbl, inputs)
% Make an automatic guess for the starting parameters if the starting guess
% is empty
if isempty(inputs.startingGuess)
	% Calculate weights
	w = tbl.od - min(tbl.od) + eps;
	w = w/max(w);
	
	% Calculate weighted centroid
	xCenter = sum(tbl.X.*w)/sum(w);
	yCenter = sum(tbl.Y.*w)/sum(w);
	
	xRadius  = max(abs(tbl.X(w > 0.1) - xCenter));
	yRadius  = max(abs(tbl.Y(w > 0.1) - yCenter));
	
	volume = 2*pi/5*max(tbl.od)*xRadius*yRadius;
    if inputs.fitOffset
      offset = mean(mean(inputs.thermalInput));
   else
      offset  = 0;
   end
   
elseif isstruct(inputs.startingGuess)
	% Parse starting guess input as struct
	volume = inputs.startingGuess.amplitude;
	xCenter	  = inputs.startingGuess.xCenter;
	yCenter   = inputs.startingGuess.yCenter;
	xRadius   = inputs.startingGuess.xWidth;
	yRadius   = inputs.startingGuess.yWidth;
	if isfield(inputs.startingGuess, 'offset')
		offset = inputs.startingGuess.offset;
	end
elseif isdouble(inputs.startingGuess)
	volume = inputs.startingGuess(1);
	xCenter	  = inputs.startingGuess(2);
	yCenter   = inputs.startingGuess(3);
	xRadius    = inputs.startingGuess(4);
	yRadius    = inputs.startingGuess(5);
	if inputs.fitOffset
		offset = inputs.startingGuess(6);
	end
else
	error('Could not parse starting guess input')
end

if inputs.fitOffset
	startingGuess = [volume, xCenter, xRadius, yCenter, yRadius, offset];
else
	startingGuess = [volume, xCenter, xRadius, yCenter, yRadius];
end
	
end
function [model, coefficientNames] = selectFittingmodel(inputs)
if inputs.fitOffset
	model = @(p,tbl) 5*p(1)/(2*pi*p(3)*p(5)) * max(1 -(tbl(:,1) - p(2)).^2/p(3)^2 - (tbl(:,2) - p(4)).^2/p(5)^2, 0).^(3/2) + p(6);
	coefficientNames = {'Volume', 'xCenter', 'xRadius', 'yCenter', 'yRadius', 'offset'};
	
else
	model = @(p,tbl) 5*p(1)/(2*pi*p(3)*p(5)) * max(1 -(tbl(:,1) - p(2)).^2/p(3)^2 - (tbl(:,2) - p(4)).^2/p(5)^2, 0).^(3/2);
	coefficientNames = {'Volume', 'xCenter', 'xRadius', 'yCenter', 'yRadius'};
end

end