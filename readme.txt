Main author: Jeppe Thuesen
Latest author: Still the above

Latest readme update: 02/05-19
Latest app update: 02/05-19

(when the word "app" is used, it might refere to the MainInterface.mlapp file)
("Program" is used to denote all the files in this folder)

**START UP***

The program can be opened either by running the MainInterface.mlapp file or by running the 
dawn.m file. dawn.m files can be modified to have your chosen startup values by adding a few lines
in your dawn.m file and in inputFunc.m

If you want to change some of the default settings in the "empty" startup screen (e. g. Lab or drive)
You can easily do this, either by adding some lines in the dawn.m file and then add the reverse lines 
in inputFunc.m or by opening the GUI with app designer and simply changing it there (requires matlab R2018 or later,
this solution might not be the best, as the first is more robust to updates as both dawn.m and inputFunc.m is 
ignored in Sourcetree.

*** Running notes ***

 -  If the program is running a string of pictures and the user wants to stop it from evaluation the next picture,
simply take the mode back to the "Single image"-mode.

 - In order for Manual RoI to accept the selected RoI, double click inside the selected area.

 - The update button in the Settings tab is only for saving the values for later use. The program will still 
run with the changed values, however at a reboot of the program, the value will revert, unless "Update" is 
pressed.

 - Rotation is implemented for HiRes, if you want to use the rotation, be sure to have the same rotation for 
both the roi and for the evaluation itself. 

*** Camera Notes ***

 - cameraTable.txt is meant as a place to store your camera data. In principle camera specific things 
like camera calibration and pixelsize is saved here and then loaded. At start up the last option in 
cameraTable.txt is chosen as the default value.

*** Meaning of the UI "light" ***

 - Green: The program is idle and waiting for a command from its Overlord.
 - Yellow: The program is searching for the next picture.
 - Red:  Program is running 
 - Black: An error have happened


*** Important notes ***

 -If you want to edit something in MainInterface.mlapp, you need Matlab R2018b or newer.

 -The general idea for the complete program, is that the app gets all the different inputs
either by direct input or by calling some different .m functions. First the app prepares all the static things
in app.evaluationStartAndPrep, then app.evaluationMain handles all inputs that a user might change from run to run.
Then UltracoldImage.m takes care of the evaluation itself. This file can also be used independantly from SkyFit.
Finally app.evaluationMain takes care of showing the correct results the correct place.

*** Fit Offset ***

 - Do we want the fits to have an offset or not. Tests of the plot(tx/tc,nbec/nsum) have shown that at small
RoIs it seems better with the offset off, but for large RoIs it seems like the offset is the best solution.

*** BEC RoI method explanation ***

 - 1D calculations first takes all pixels in the roi defined from "auto" or "manual" roi, then fits a gaussian
to find the center of the BEC+thermal cloud. This is under the assumption that both the BEC and thermal cloud have
the same center. Then a cross is made with intersection in this center. The cross is 3 pixels wide. 
Then by doing a combined thermal and BEC fit, the BEC roi is then calculated. 

 - Load previous just uses the results the last time the BEC roi was defined.

*** Center Boundness ***
 - This where the method used to find the center of both the BEC and thermal cloud. If "Bound" is selected
then the method chosen in "BEC Roi Method" gives the center and this is used as a value when doing the 
BEC and Thermal Fit later on. If on the other hand "Fitted" is used, then each fit have the center as two
more fitting parameters, which might lead to the thermal cloud having another center then the BEC.
Tests have shown different options here leads to a ~5% change in temperature, but not anything 
note-worthy in the other results.

*** Validity of fits ***
 - The program gives the SSE of both the thermal and BEC fit, which tells something about the goodness of the
fit. Another measure is the midPointsError which is a measure of how much the value of the two fits combined
in the center differs from the actual data center points. This number can be compared to what is seen in the two
flank figures, it is however a bad way to determine the error...


*** MainInterface.mlapp Notes ***

 -This is made in Matlab's integrated App Designer.

 - MainInterface.mlapp consists of a number of "callback" functions assigned to different components
in the GUI, all of these serve to find the different parameters wanted in an evaluation.

*** "Hack" Notes ***
 -It is possible to "hack" the program by going around the GUI, this is done in lattice_hacking.m. An easy way
to do this would be to run the program once in order to set the settings as wanted. As the app is saved in the
workspace it is possible to manipulate the app outside it. However these hacks tend to chrash the app after a 
while..


*** Misc ***
- "Easter egg" explination: The program have evolved and are somewhat sentient. A self-destruct from 
time to time would ensure that SkyFit doesn't evolve to SkyNet. Yes that is part of the reason for 
the somewhat weird name. But hey, I didn't see you come up with a better one?

 - Are there really anyone reading this at any point in time? 

 - Why is readme-files always .txt-files? Or are they?