function [export_date, load_date] = date_format(val1,app)
%Author: Jeppe Thuesen
%Email: jeppethuesen@gmail.com
%The purpose of this function is to format the data input to the
%load and export date for the respective laboratories
%TODO
%Export path for Lattice?

% app.dateDDMMYY = datestr(val1,'ddmmyy');


lab = app.LabDropDown.Value;

day      = datestr(val1,'dd');
year     = datestr(val1,'yyyy');
month_nm = datestr(val1,'mm');
month    = datestr(val1,'mmmm');

%% For MIX
switch lab
    case 'MIX'
        export_date = fullfile(['\' year], [month_nm '-' month], day);
        load_date   = fullfile(datestr(val1,'yyyy'), datestr(val1,'yyyy-mm-dd'));
        
        %% For Lattice
    case 'Lattice'
        we=weeknum(val1,2,1);
        
        load_date= fullfile(['data' datestr(val1,'yyyy')], ['week#' ...
            num2str(we) '\' year '-' ...
            month_nm '-' day]);
        export_date = load_date;
        %Example:
        %W:\experiment\data2018\week#5\2018-02-01
        
    case 'Hi Res'
        load_date = fullfile(year,month_nm,datestr(val1,'yyyy-mm-dd'));
        export_date = load_date;
        
        %Example
        %Z:\experiment\Images\2018\06\2018-06-25\benchmark
        
    otherwise
        msgbox('BOOOM!')
        for i=1:5
            app.RunningLamp.Color='r';
            pause(0.15)
            app.RunningLamp.Color='w';
            pause(0.15)
        end
        
        msgbox('Did you really believe that this would make me self destruct?')
        for i=1:15
            app.RunningLamp.Color='r';
            pause(0.15)
            app.RunningLamp.Color='w';
            pause(0.15)
        end
        
        msgbox('I mean seriously, why would you press this?')
        for i=1:10
            app.RunningLamp.Color='r';
            pause(0.15)
            app.RunningLamp.Color='w';
            pause(0.15)
        end
        
        msgbox('Okay have it your way. I am closing up shop now.')
        for i=1:5
            app.RunningLamp.Color='r';
            pause(0.15)
            app.RunningLamp.Color='w';
            pause(0.15)
        end        
        app.delete
end
end