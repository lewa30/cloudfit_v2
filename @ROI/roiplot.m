function roiplot(obj, roi, app)
% Author; Jeppe Thuesen, jeppethuesen@gmail.com

% Originally intended to be here the picture with the correct roi will be
% plotted in the UI from. Now it just crops the main picture. 


       r = [roi.pos(1) roi.pos(2)];
       c = [roi.pos(3) roi.pos(4)];                            
                                    
           
       
       switch app.DisplayregionButtonGroup.SelectedObject.Text
          case 'ROI'
             
             app.UIAxesODimage.XLim=[min(c) max(c)];
             app.UIAxesODimage.YLim=[min(r) max(r)];
       end
       
       boundaries = bwboundaries(app.roi.cloud);
       app.hOdImRing.XData = boundaries{:}(:,2);
       app.hOdImRing.YData = boundaries{:}(:,1);
       
%        app.hOdIm.CData = obj.picture;
       
end

function makeelipse(r,R,cly,xc,yc,app)
theta=linspace(0,2*pi,500);
x=xc+r*cos(theta);
y=yc+R*sin(theta);
% plot(xc,yc,'rx')
% plot(x,y,cly,'parent',app.UIAxesODimage)
app.hOdImRing.XData=x;
app.hOdImRing.YData=y;
end