function autowidthRoi(obj, raw, xRadius, yRadius)
%{
Author: Mick Kristensen
        Jeppe Thuesen (latest editor), email: jeppethuesen@gmail.com

        
        

  TODO: make a change in thermal scaling depending on the given lab?

Note:
  

%}

% obj.image = roipoly(raw.atoms, [edgePixels(1) raw.width-edgePixels(2) raw.width-edgePixels(2) edgePixels(1)], ...
%     [edgePixels(3) edgePixels(3) raw.height-edgePixels(4) raw.height-edgePixels(4)]);



% Calculate normalized image
od = log(raw.beam./raw.atoms).*obj.image;
% app.hOdIm.CData = w;
% w = w - min(w(:)) + eps;
od = od.*(od>0);
od = od/max(od(:));
obj.picture = od;
% Calculate grid
[X, Y]  = meshgrid(1:raw.width, 1:raw.height);
obj.centerRoI(od,X,Y,0.8) ;

center = [obj.centerX, obj.centerY];






%This is done in ultracoldImage.m instead.
% obj.bec		= obj.estimateRoi(w, X, Y, becThreshold, becScaling,xCenter,yCenter);
% obj.thermal = obj.cloud & ~obj.bec;


% diff_reference=obj.image & ~obj.cloud;







% becRoiCalc(obj, od, X, Y, semi, maxOD)

%This value is somewhat arbitrary, it is done here
%as to not send obj.od into roiRadiusCalc.m
% thermalStartGuess = mean(abs(od(obj.cloud)));
%
%             [obj.centerX, obj.centerY]...
%              = obj.centerRoI(obj.picture,obj.X,obj.Y,threshold);
% xColumn = sum(od.*obj.image,1);
% yRow    = sum(od.*obj.image,2);
% 
% %This is to use a stable way to find the center by
% %fitting to a 1D gaussian. One underlying assumption is
% %that the BEC and thermal cloud have the same center.
% fitX = fit(X(1,:)',xColumn','gauss1');
% fitY = fit(Y(:,1),yRow,'gauss1');
% 
% % obj.centerX = fitX.b1;
% % obj.centerY = fitY.b1;
% 
% % centerFit = [obj.centerX, obj.centerY]
% 
% 
% %             obj.roiradiusCalc(thermalStartGuess,obj.semi);
% 
% 
% 
% 
% becScaling = 1.0;
% pixel_amount = 1;
% %The center needs to be a whole integer. As centerX and centerY is
% %calculated this might not naturally be just that.
% Xcenter = round(obj.centerX);
% Ycenter = round(obj.centerY);
% 
% %A cross is made that is 2*pixel_amount+1 wide, with the intersection at
% %(centerX,centerY). This is done for to ensure that noise plays a little
% %part.
% X_pixels = Xcenter-pixel_amount:Xcenter+pixel_amount;
% Y_pixels = Ycenter-pixel_amount:Ycenter+pixel_amount;
% 
% 
% yy = mean(obj.picture(:,X_pixels)');
% yx = 1:size(obj.picture,1);
% 
% xy = mean(obj.picture(Y_pixels,:));
% xx = 1:size(obj.picture,2);



theta   = linspace(0,2*pi,1000);
x       = obj.centerX + xRadius*cos(theta);
y       = obj.centerY + yRadius*sin(theta);



obj.cloud = obj.image & poly2mask(x,y, raw.height, raw.width);

referenceScale = 1.3;

xRef       = obj.centerX + xRadius*referenceScale*cos(theta);
yRef       = obj.centerY + yRadius*referenceScale*sin(theta);

obj.reference = obj.image & ~poly2mask(xRef, yRef, raw.height, raw.width);

[r c] = find(obj.cloud);
x1 = min(r);
x2 = max(r);
y1 = min(c);
y2 = max(c);

obj.pos = [x1 x2 y1 y2];


end