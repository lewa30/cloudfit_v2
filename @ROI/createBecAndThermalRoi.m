function createBecAndThermalRoi(obj,becRoiMethod,BecOn, od, X, Y,semi, maxOD)
%This function is for calculating the roi of the BEC by a making a fit of
%the data points on both the X and Y axis that crosses in the center.

%Author: Jeppe J. Thuesen, jeppethuesen@gmail.com


switch becRoiMethod
    
    case '1D calculation'
        if BecOn == 1
            becRoiCalc(obj,od, X, Y, semi, maxOD)
            
        else
            obj.bec     = zeros(size(obj.image));

            obj.thermal = obj.cloud & od <maxOD;

            obj.thermal = obj.cloud & od < maxOD;

        end
    case 'Load previous'
        %as the old BEC roi is already passed, this is left
        %blank, as to not overwrite the previous one.
end

end

function becRoiCalc(obj, od, X, Y, semi, maxOD)

%This value is somewhat arbitrary, it is done here
%as to not send obj.od into roiRadiusCalc.m
thermalStartGuess = mean(abs(od(obj.cloud)));
%
%             [obj.centerX, obj.centerY]...
%              = obj.centerRoI(obj.picture,obj.X,obj.Y,threshold);
xColumn = sum(od.*obj.cloud,1);
yRow    = sum(od.*obj.cloud,2);

%This is to use a stable way to find the center by
%fitting to a 1D gaussian. One underlying assumption is
%that the BEC and thermal cloud have the same center.
fitX = fit(X(1,:)',xColumn','gauss1');
fitY = fit(Y(:,1),yRow,'gauss1');

obj.centerX = fitX.b1;
obj.centerY = fitY.b1;



%             obj.roiradiusCalc(thermalStartGuess,obj.semi);




becScaling = 1.0;
pixel_amount = 50;
%The center needs to be a whole integer. As centerX and centerY is
%calculated this might not naturally be just that.
Xcenter = round(obj.centerX);
Ycenter = round(obj.centerY);

%A cross is made that is 2*pixel_amount+1 wide, with the intersection at
%(centerX,centerY). This is done for to ensure that noise plays a little
%part.
X_pixels = Xcenter-pixel_amount:Xcenter+pixel_amount;
Y_pixels = Ycenter-pixel_amount:Ycenter+pixel_amount;


yy = mean(obj.picture(:,X_pixels)');
yx = 1:size(obj.picture,1);

xy = mean(obj.picture(Y_pixels,:));
xx = 1:size(obj.picture,2);

%We choose the model we fit after. If BEC is chosen as off, this
if semi == false
    thermalModel = 'p4*exp( -(x-p2).^2/(2*(p5+p3)^2) )';
else
    thermalModel = ['p4*0+p5*0'];
end
% thermalModel = ['p4*0+p5*0'];

TFModel = 'p1*max(0,(1 - ((x-p2)/p3).^2 )).^(3/2)';
% p1: Amplitude BEC
% p2: Center Cloud
% p3: TF radius BEC
% p4: Amplitude Thermal
% p3 + p5: Width Thermal, constrained to be larger than TF radius
% p6: Offset


%     model = [thermalModel '+' TFModel '+p6'];
model = [thermalModel '+' TFModel];


% intermediateFitX=fit(xx',xy',model, ...
%     'StartPoint', [max(xy), xx(xy==max(xy)), 10, thermalstartGuess, ...
%     (roi.pos(4)-roi.pos(3))/2, 0], ...
%     'Lower', [0, 0, 0, 0, 0, -Inf] );
%
% intermediateFitY=fit(yx',yy',model, ...
%     'StartPoint', [max(yy), yx(yy==max(yy)), 10, thermalstartGuess, ...
%     (roi.pos(2)-roi.pos(1))/2, 0], ...
%     'Lower', [0, 0, 0, 0, 0, -Inf] );

intermediateFitX=fit(xx',xy',model, ...
    'StartPoint', [max(xy), min(xx(xy==max(xy))), 10, thermalStartGuess, ...
    (obj.pos(4)-obj.pos(3))/2], ...
    'Lower', [0, 0, 0, 0, 0] );

intermediateFitY=fit(yx',yy',model, ...
    'StartPoint', [max(yy), min(yx(yy==max(yy))), 10, thermalStartGuess, ...
    (obj.pos(2)-obj.pos(1))/2], ...
    'Lower', [0, 0, 0, 0, 0] );

fitCenterX = intermediateFitX.p2;
fitCenterY = intermediateFitY.p2;

becXRadius = becScaling*intermediateFitX.p3;
becYRadius = becScaling*intermediateFitY.p3;

[height width] = size(obj.picture);
obj.bec = masking(becXRadius, becYRadius, fitCenterX, fitCenterY, height, width) ...
          & od < maxOD & obj.cloud;

obj.thermal = obj.cloud & ~obj.bec & od < maxOD;

end



function mask = masking(rx, ry, rx0, ry0, height, width)


theta   = linspace(0,2*pi,1000);
x       = rx0 + rx*cos(theta);
y       = ry0 + ry*sin(theta);

mask = poly2mask(x,y, height, width);

end