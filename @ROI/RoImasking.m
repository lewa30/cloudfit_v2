function maskedRoI = RoImasking(obj, pos, height, width, scaling)

rx = (pos(4)-pos(3))/2;
ry = (pos(2)-pos(1))/2;




% xCenter  =  sum(X(mask).*w(mask))/sum(w(mask));
% yCenter  =  sum(Y(mask).*w(mask))/sum(w(mask));
rx0 = pos(3) + rx;
ry0 = pos(1) + ry;


theta   = linspace(0,2*pi,1000);
x       = rx0 + scaling*rx*cos(theta);
y       = ry0 + scaling*ry*sin(theta);

maskedRoI = poly2mask(x,y, height, width);%ceil(pos(3)), ceil(pos(4)));


% sum(sum(mask))
% max(max(mask))



%   distance = sqrt((rx-rx0).^2+(ry-rx0).^2);
% % 
%   background_pixels = w(distance<x & distance<y);
%   figure('name', 'baggrund')
%   imagesc(background_pixels)

 

end