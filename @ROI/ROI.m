classdef ROI < handle
   %UNTITLED Summary of this class goes here
   %   Detailed explanation goes here
   
   properties (Access = public)
      % Masks
      image
      cloud
      reference
      bec
      thermal
      
      % Raw data
      picture
      
      % Center
      centerX
      centerY
      
      % Outer positions
      pos
      
      % Size of roi
      height
      width
   end
   
   properties (Access = private)
      center
      referenceRadii
      thermalRadii
      becRadii
      edgePixels
   end
   
   %     properties (Dependent)
   %         heightWidthPosition
   %     end
   %% Constructor
   methods
      function obj = ROI(varargin)
         % Skyfit uses the ROI class without the constructor
         if isempty(varargin)
            return
         end
         
         % Setup inputparser
         p = inputParser;
         p.FunctionName = 'ROI';
         
         % Validation functions
         chkVector = @(x,size) validateattributes(x, {'numeric'}, {'finite', 'numel', size});
         
         % Add parameters to the input parser
         p.addParameter('center',         [], @(x) chkVector(x, 2))
         p.addParameter('referenceRadii', [], @(x) chkVector(x, 2))
         p.addParameter('thermalRadii',   [], @(x) chkVector(x, 2))
         p.addParameter('becRadii',       [], @(x) chkVector(x, 2))
         p.addParameter('edgePixels',      0, @(x) any(numel(x) == [1 2 4]))
         p.addParameter('width',          [], @(x) chkVector(x, 1))
         p.addParameter('height',      0, @(x) chkVector(x, 1))
         p.addParameter('pos',          [], @(x) chkVector(x, 0))
         
         % Parse the inputs
         p.parse( varargin{:} );
            
         % Assign parser result to object properties
         parameterNames = fieldnames(p.Results);
         nfileds        = numel(parameterNames);
         for ifield = 1:nfileds
            obj.(parameterNames{ifield}) = p.Results.(parameterNames{ifield});
         end
         
         % Create regions of interest
         obj.image     = obj.createRectangularMask(obj.edgePixels, obj.width, obj.height);
         obj.reference = obj.image & ~obj.createEllipticalMask(obj.center, obj.referenceRadii,obj.width, obj.height);
         obj.bec       = obj.image & obj.createEllipticalMask(obj.center, obj.becRadii, obj.width, obj.height);
         obj.thermal   = obj.image & ~obj.bec & obj.createEllipticalMask(obj.center, obj.thermalRadii, obj.width, obj.height);
         obj.cloud     = obj.createEllipticalMask(obj.center, obj.thermalRadii, obj.width, obj.height) | obj.thermal | obj.bec;
         
         obj.pos = [obj.center(2)-obj.thermalRadii(2), obj.center(2)+obj.thermalRadii(2),...
         obj.center(1)-obj.thermalRadii(1), obj.center(1)+obj.thermalRadii(1)];

         obj.centerX = obj.center(1);
         obj.centerY = obj.center(2);


      end
   end
   
   
   %% Other methods
   methods (Access = public)
      % Declaration of methods in separate files
      createBecAndThermalRoi(obj,becRoiMethod,BecOn, od, X, Y,semi, maxOD)
      
      autoRoi(obj, raw, edgePixels, rot)
      
      centerRoI(obj, w, X, Y, threshold)
      
      selectedRoI = estimateRoi(obj, w, X, Y, threshold, scaling,xCenter,yCenter)
      
      manualRoi(obj, raw, edgePixels, app,resizeParameter, rot)
      
      maskedRoI = RoImasking(obj, pos, height, width, scaling)
      
      roiplot(obj, roi, app)
      
      autowidthRoi(obj, raw, xRadius, yRadius)
   end
   
   %% Private methods
   methods (Static)
      
      
      function mask = createEllipticalMask(center, semiaxes, width, height)
         theta   = linspace(0,2*pi,1000);
         x       = center(1) + semiaxes(1)*cos(theta);
         y       = center(2) + semiaxes(2)*sin(theta);
         mask = poly2mask(x,y, height, width);
      end
      
      
      function mask = createRectangularMask(edges, width, height)
         mask    = false(height, width);
         switch numel(edges)
            case 1
               edges = [edges edges edges edges];
            case 2
               edges = [edges(1) edge(1) edges(2) edges(2)];
            case 4
               % do nothing, edges has the right format
            otherwise
               error('ROI:edges:incosistenNumberOfEdges', ...
                  "Edge specification must be a vector of length 1, 2 or 4 and follow one of the formats:" ...
                  + "[edges], [sides top&bottom] or [left right top bottom]")
         end
         mask(edges(3) + 1:height - edges(4), edges(1) + 1:width - edges(2)) = true;
      end
   end
   
   methods
      %         function result = get.heightWidthPosition(obj)
      %             p = obj.pos;
      %             result = [p(1) p(3) p(2)-p(1) p(4)-p(3)];
      %         end
   end
end

