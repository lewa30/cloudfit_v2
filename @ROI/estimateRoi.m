function roi = estimateRoi(obj, w, X, Y, threshold, scaling,xCenter,yCenter)
% Set mask for thermal cloud
mask = w > threshold;

% figure
% subplot(221)
% imagesc(w)
% subplot(224)
% imagesc(mask)

% We need to purify this region of interest
% % Calculate weighted centroid
% xCenter = sum(X(mask).*w(mask))/sum(w(mask));
% yCenter = sum(Y(mask).*w(mask))/sum(w(mask));
        %Outcommented as a different center is taken in another function
        %now


% Calculate diameter of circle with same area as the mask, and average
% distance from centroid
radius   = sqrt(sum(mask(:))/pi);
xDist    = mean(abs(X(mask) - xCenter));
yDist    = mean(abs(Y(mask) - yCenter));
ecc      = xDist/yDist;

% Draw an assymmetric circular roi
t		 = linspace(0, 2*pi, 1000);
xroi	 = xCenter + scaling*radius*ecc*cos(t);
yroi     = yCenter + scaling*radius/ecc*sin(t);
roi		 = poly2mask(xroi, yroi, size(mask, 1), size(mask, 2));
end