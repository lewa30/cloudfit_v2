function centerRoI(obj, w,X,Y,threshold)
% Calculating the center from the BEC parametres
%NOTE the center of the BEC is not necessary the center of the thermal 
%cloud, however this is just a RoI, so it is deemed ok. The reason for 
%using the BEC center for the RoI-center is that this is more 
%clearly defined than the thermal cloud
mask = w > threshold;


obj.centerX = sum(X(mask).*w(mask))/sum(w(mask));
obj.centerY = sum(Y(mask).*w(mask))/sum(w(mask));

end