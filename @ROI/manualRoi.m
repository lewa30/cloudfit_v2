function manualRoi(obj, raw, edgePixels,app,resizeParameter,rot) 
%{ 
Author: Jeppe Thuesen 
email: Jeppethuesen@gmail.com
%}
lower = app.EditField_ColourlimitsLower.Value;
upper = app.EditField_ColourlimitsUpper.Value;

cameraCalib   = app.parameters.cameraCalib;
alpha       = app.parameters.alpha;
detectionTime = app.parameters.detectionTime;


% These are also defined in app.PosEditFieldValueChanged(app, event), so 
% if for some reason you want them changed, remember to also do it there 
cloudScale     = 1;
% becScaling     = 1.8;
referenceScale = 1.3;




%Calculated intermediate od. Intermediate as it does not use the roi
%reference, which is done later in UltracoldImage.m
	atoms = cameraCalib*(raw.atoms - raw.atomBias)./detectionTime;
	beam  = cameraCalib*(raw.beam - raw.beamBias)./detectionTime;
	
   
	% If there is zero-counts in the image the image we cannot calculate
	% od, so we add eps
    atoms(atoms == 0) = eps;
    beam(beam == 0) = eps;
    
    % Calculate ratio between atom and beam image and rescale coefficient
    ratio = mean(atoms(obj.image))./mean(beam(obj.image));
    beam  = beam*ratio;
    
    delta = log(abs(beam./atoms));
    
    if isnumeric(alpha) == 0
        func = alpha;
       alpha = arrayfun(func,delta); 
    end
    
    w = alpha.*delta + (beam - atoms);
    w = w.*obj.image;

% Calculate normalized image
% w = log(raw.beam./raw.atoms).*obj.image;
% app.hOdIm.CData = w;
% 
% w = w - min(w(:)) + eps;
% 
% % w = w.*(w>0);
% w = w/max(w(:));
% % w=rescale(w);



figure('Name','Manual')
A=axes;
obj.picture = w;
J = imresize(w,resizeParameter);
% J = imadjust(w);

pic = image(A,J);
% pic.CData=w;
pic.CDataMapping = 'scaled';
A.CLim = [lower, upper];
A.YDir = 'normal';

% imcontrast(pic)

H         = imellipse(gca);
position  =  wait(H);


pos = getPosition(H)/resizeParameter;
obj.pos=[pos(2) pos(2)+pos(4) pos(1) pos(3)+pos(1)];

 

obj.cloud = obj.RoImasking(obj.pos, raw.height, raw.width, cloudScale);
obj.reference = obj.image & ~obj.RoImasking(obj.pos, raw.height, raw.width, referenceScale);
% obj.reference = obj.image & ~masking(pos, raw.height, raw.width, referenceScale);

thres = mean(abs(w(obj.cloud))); %This could mabe be times 2? 

[X, Y]  = meshgrid(1:raw.width, 1:raw.height);
obj.centerRoI(w,X,Y,thres) ;

%  obj.bec		= obj.estimateRoi(w, X, Y, becThreshold, becScaling,obj.centerX,obj.centerY);
% obj.thermal = obj.cloud & ~obj.bec;


%Do we want it to close after selecting obj.cloud?
close Manual


end


