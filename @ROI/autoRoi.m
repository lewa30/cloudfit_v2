function autoRoi(obj, raw, edgePixels,rot)
%{
Author: Mick Kristensen
        Jeppe Thuesen (latest editor), email: jeppethuesen@gmail.com

        
        

  TODO: make a change in thermal scaling depending on the given lab?

Note:
  

%}
%requirement: referenceScaling>thermalScaling
%Otherwise we might see a overlap in the obj.reference and obj.cloud and
%that we do NOT want to see. Bad science.
thermalScaling = 2;
% becScaling = 1.8;
referenceScaling = 2.5;

thermalThreshold = 0.1;
becThreshold = 0.7;


% obj.image = roipoly(raw.atoms, [edgePixels(1) raw.width-edgePixels(2) raw.width-edgePixels(2) edgePixels(1)], ...
%     [edgePixels(3) edgePixels(3) raw.height-edgePixels(4) raw.height-edgePixels(4)]);



% Calculate normalized image
w = log(raw.beam./raw.atoms).*obj.image;
% app.hOdIm.CData = w;
% w = w - min(w(:)) + eps;
w = w.*(w>0);
w = w/max(w(:));
obj.picture = w;
% Calculate grid
[X, Y]  = meshgrid(1:raw.width, 1:raw.height);
obj.centerRoI(w,X,Y,0.6) ;




if thermalThreshold==0
    thermalThreshold = estimateThermalThreshold(w, X, Y, becThreshold,obj.centerX,obj.centerY);
    
end
obj.cloud	= obj.estimateRoi(w, X, Y, thermalThreshold, thermalScaling,obj.centerX,obj.centerY);
obj.reference	= obj.image & ~obj.estimateRoi(w, X, Y, thermalThreshold, referenceScaling,obj.centerX,obj.centerY);

%This is done in ultracoldImage.m instead.
% obj.bec		= obj.estimateRoi(w, X, Y, becThreshold, becScaling,xCenter,yCenter);
% obj.thermal = obj.cloud & ~obj.bec;

[r c]=find(obj.cloud);
x1=min(r);
x2=max(r);
y1=min(c);
y2=max(c);

obj.pos=[x1 x2 y1 y2];

% diff_reference=obj.image & ~obj.cloud;



end




function ThermalCalc= estimateThermalThreshold(w, X, Y, threshold,xCenter,yCenter)

mask = w > threshold;

% xCenter = sum(X(mask).*w(mask))/sum(w(mask));
% yCenter = sum(Y(mask).*w(mask))/sum(w(mask));
r=70;
R=r+25;
% figure
imagesc(w)
hold on
makecirc(r,'g--',xCenter,yCenter);
makecirc(R,'g--',xCenter,yCenter);
distance = sqrt((X-xCenter).^2+(Y-yCenter).^2);
%
background_pixels = w(distance>r & distance<R);
% summation_of_pixels=sum(background_pixels);
% ThermalCalc=mean(background_pixels)
ThermalCalc = mean(background_pixels(background_pixels>0))

% ThermalCalc=1;

end



function makecirc(r,cly,xc,yc)
theta=linspace(0,2*pi,50);
x=xc+r*cos(theta);
y=yc+r*sin(theta);
plot(xc,yc,'rx')
plot(x,y,cly)
end