%% 3D visualisation

trap_x = 93.4;
trap_y = 17.7;
trap_z = 93.4;

% X = app.UltracoldImageEval.X;
% Y = app.UltracoldImageEval.Y;



xc = app.UltracoldImageEval.results.xPos;
yc = app.UltracoldImageEval.results.yPos;
zc = 0;

xr = app.UltracoldImageEval.foThermal.Coefficients{'xWidth','Estimate'};
yr = app.UltracoldImageEval.foThermal.Coefficients{'yWidth','Estimate'};

scaleFactor = trap_z/trap_x;
zr = xr*scaleFactor;

n = length(app.UltracoldImageEval.X);



f = figure;
% C = winter;
[x,y,z] = ellipsoid(xc,yc,zc,xr,yr,zr,n);

S = surf(x,y,z);
set(S,'LineStyle','none');




%% non ellipsoid

Zgrid = app.roi.picture.*app.roi.cloud;
% Zgrid(Zgrid<0.05) = 0;

Zgrid(Zgrid == 0) = NaN;

% Zgrid = reshape(Zgrid,size(app.roi.cloud));

ZgridPixels = Zgrid./max(max(Zgrid)).*min(size(app.UltracoldImageEval.X)).*trap_z/trap_x*1/2;



fig = figure;


gridPlus = surf(ZgridPixels,'CData',Zgrid);
hold on
gridMinus = surf(-ZgridPixels,'CData',Zgrid);

set(gridPlus,'LineStyle','none');

set(gridMinus,'LineStyle','none');