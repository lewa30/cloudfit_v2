p% This file makes it possible to evaluate a bunch of images based on which
% are evaluated in an old file.



tt = absorptionFits2018Jan27run697FixedROI.Time;

dayString = dateshift(tt, 'start', 'day');
dayString.Format = 'dd-MMM-yyyy';

% dayString = ['290118'] ;% '160618'];

runs = absorptionFits2018Jan27run697FixedROI.run; %, 611:612] ; 1:2};


fileName = 'largeTestColdFixwOffset';

app.FileCheckBox.Value = true;
app.FilenameEditField.Value = fileName;

fileLocator = struct;

% dayString = [tt(2); tt(3)];
% runs = [runs(2), runs(3)];
fileLocator.day = dayString(100:700);
fileLocator.run = runs(100:700);

try 
    alreadyRun = readtable(fullfile('testfiles',[fileName, '.txt']));
    inHouse = size(alreadyRun,1);
catch
    inHouse = 0;
end

theHackLoop(fileLocator,app,inHouse)




function theHackLoop(fileLocator,app,inHouse)

for i=2+inHouse:length(fileLocator.run)

% ds = FileIO.Dataset(dayString(i,:), runCell{i}); %291:311 restarts the macro scan.
%     ds.times; % Prompt the object to read the ecs-times.
%     save dataset ds
    
%     dateSerial = datenum(fileLocator.day, 'ddmmyy'); %This is a serial code
                                                    %assigned to the date
    app.ModelButtonGroup.SelectedObject.Text = 'Bimodal';
     app.DateEditField.Value = datestr(fileLocator.day(i),'dd-mmm-yyyy');
     [~, load_date] = date_format(fileLocator.day(i),app);
     app.dateDDMMYY = datetime(fileLocator.day(i));
     
     
     app.ImportpathEditField.Value = fullfile('D:\experiment',load_date);
     
     
     run = fileLocator.run(i);
     
     
    
     app.StartnumberEditField.Value = run;
     
     
%      addpath('C:\Users\au511366\Dropbox\Fysik - studie\speciale\SkyFit');
    %SkyFit location, CHANGE FOR GENERAL CASE AT SOME POINT
     app.evaluationMain
     if app.UltracoldImageEval.results.NBEC<0
         app.ModelButtonGroup.SelectedObject.Text = 'BE Thermal';
         app.evaluationMain
     end

end
end