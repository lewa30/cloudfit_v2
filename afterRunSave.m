function afterRunSave(app,exportFolder)
%This function is where the settings deemed valuable to save, in order to
%load them again in the event of a chrash or if the precise details for a
%run is wanted again.
disp('saving')


%If you want more settings to be saved, simply add them here
saved.roi         = app.roi;
saved.BECmethod   = app.BECRoImethodButtonGroup.SelectedObject.Text;
saved.centerBound = app.CenterBoundnessButtonGroup.SelectedObject.Text;
saved.fitOffset   = app.FitOffsetCheckBox.Value;
saved.fitModel    = app.ModelButtonGroup.SelectedObject.Text;
saved.constantTab = app.Constants.Data;
saved.alpha       = app.parameters.alpha;
saved.camCalib    = app.parameters.cameraCalib;

%These two are not loaded into the GUI. They are only here as failsafe check.
saved.startNum    = app.StartnumberEditField.Value;
saved.date        = app.DateEditField.Value;

exportPath = fullfile(exportFolder,'appSettings');
if exist(exportPath) == 0
    mkdir(exportPath)
end
exportName = fullfile(exportPath,['run', num2str(app.StartnumberEditField.Value), '_settings']);

savepart(saved,exportName)




end


function savepart(saved,exportName)
%This might not be the best way to save the file, but it works.
save(exportName)

end