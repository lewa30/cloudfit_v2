function polylog3LUT = createPolylog2Lookup
%% polylog2LUT = createPolylog2Lookup
%  Create a lookup table to evaluate the Polylogarithm/Bose function of
%  second order on the interval [0 ; 1]
% 
% pathname = mfilename('fullpath');
% pathname = fullfile(pathname,'..','polylog2LUTdata.mat');
pathname = mfilename('fullpath');
pathname = fileparts(pathname);
filename = fullfile(pathname,'polylog2LUTdata.mat');

if exist(filename,'file') == 2
	load(filename)
else
	x = linspace(0,1,30000);
	y = polylog(2,x);
	save(filename,'x','y')
end
polylog3LUT = @(xi) interp1(x,y,xi,'pchip');
end