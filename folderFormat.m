function imageFolder = folderFormat(app)
%Author: Jeppe J. Thuesen

drive = app.DriveDropDown.Value;
lab   = app.LabDropDown.Value;
if app.AutomaticpathCheckBox.Value == 0
    imageFolder = app.ImportpathEditField.Value;
else
    
    switch lab
        case 'MIX'
            if app.StationaryCheckBox.Value == 1
                imageFolder = fullfile([drive app.load_date ' andor']);
            else
                imageFolder = fullfile(drive, 'Images', [app.load_date ' andor']);
            end
        case 'Lattice'
            imageFolder = fullfile(drive, 'experiment', app.load_date);
            
        case 'Hi Res'
            imageFolder = fullfile(drive,'experiment','Images', app.load_date,app.prefixEditField.Value);
            %Z:\experiment\Images\2018\06\2018-06-25\benchmark
    end
end
end


